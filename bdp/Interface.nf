﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Interface))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Interface))==(Machine(Interface));
  Level(Machine(Interface))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Interface)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Interface))==(Context)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Interface))==(?)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Interface))==(Machine_Boutique,Machine_Cassette,Machine_Episode,Machine_Film,Machine_Client,Machine_Reservation,Machine_Emprunt,Machine_Possede,Machine_Contient2,Machine_Contient1,Machine_Appartient);
  List_Includes(Machine(Interface))==(Machine_Appartient,Machine_Contient1,Machine_Contient2,Machine_Possede,Machine_Emprunt,Machine_Reservation,Machine_Client,Machine_Film,Machine_Episode,Machine_Cassette,Machine_Boutique)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Interface))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Interface))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Interface))==(?);
  Context_List_Variables(Machine(Interface))==(?);
  Abstract_List_Variables(Machine(Interface))==(?);
  Local_List_Variables(Machine(Interface))==(?);
  List_Variables(Machine(Interface))==(appartient,contient1,contient2,possede,date_emp,emprunt,date_res,reservation,carte_retiree,nombre_max,adresse_cl,nom_cl,num_carte,clients,genre,titre_fl,num_fl,films,titre_ep,num_ep,episodes,num_k7,cassettes,adresse_b,nom_b,boutiques);
  External_List_Variables(Machine(Interface))==(appartient,contient1,contient2,possede,date_emp,emprunt,date_res,reservation,carte_retiree,nombre_max,adresse_cl,nom_cl,num_carte,clients,genre,titre_fl,num_fl,films,titre_ep,num_ep,episodes,num_k7,cassettes,adresse_b,nom_b,boutiques)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Interface))==(?);
  Abstract_List_VisibleVariables(Machine(Interface))==(?);
  External_List_VisibleVariables(Machine(Interface))==(?);
  Expanded_List_VisibleVariables(Machine(Interface))==(?);
  List_VisibleVariables(Machine(Interface))==(?);
  Internal_List_VisibleVariables(Machine(Interface))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Interface))==(btrue);
  Gluing_List_Invariant(Machine(Interface))==(btrue);
  Abstract_List_Invariant(Machine(Interface))==(btrue);
  Expanded_List_Invariant(Machine(Interface))==(clients <: CLIENTS & num_carte: clients >-> NAT & nom_cl: clients --> STRINGS & adresse_cl: clients --> STRINGS & nombre_max: clients --> NAT & carte_retiree: clients --> BOOL & films <: FILMS & num_fl: films --> NAT & titre_fl: films --> STRINGS & genre: films --> STRINGS & episodes <: EPISODES & num_ep: episodes --> NAT & titre_ep: episodes --> STRINGS & cassettes <: CASSETTES & num_k7: cassettes --> NAT & boutiques <: BOUTIQUES & nom_b: boutiques --> STRINGS & adresse_b: boutiques --> STRINGS);
  Context_List_Invariant(Machine(Interface))==(btrue);
  List_Invariant(Machine(Interface))==(dom(reservation)/\dom(emprunt) = {} & dom(appartient) = cassettes & dom(contient1)/\dom(contient2) = {} & dom(contient1)\/dom(contient2) = cassettes & dom(possede) = episodes & ran(contient1)/\ran(possede) = {} & appartient: cassettes --> boutiques & contient1: cassettes --> films & contient2: cassettes --> episodes & possede: episodes --> films & emprunt: cassettes --> clients & date_emp: cassettes --> NAT & reservation: cassettes --> clients & date_res: cassettes --> NAT)
END
&
THEORY ListAssertionsX IS
  Abstract_List_Assertions(Machine(Interface))==(btrue);
  Expanded_List_Assertions(Machine(Interface))==(btrue);
  Context_List_Assertions(Machine(Interface))==(btrue);
  List_Assertions(Machine(Interface))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Interface))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Interface))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Interface))==(appartient:={};contient1:={};contient2:={};possede:={};emprunt,date_emp:={},{};reservation,date_res:={},{};clients,num_carte,nom_cl,adresse_cl,nombre_max,carte_retiree:={},{},{},{},{},{};films,num_fl,titre_fl,genre:={},{},{},{};episodes,num_ep,titre_ep:={},{},{};cassettes,num_k7:={},{};boutiques,nom_b,adresse_b:={},{},{});
  Context_List_Initialisation(Machine(Interface))==(skip);
  List_Initialisation(Machine(Interface))==(skip)
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Interface))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Interface),Machine(Machine_Appartient))==(?);
  List_Instanciated_Parameters(Machine(Interface),Machine(Machine_Contient1))==(?);
  List_Instanciated_Parameters(Machine(Interface),Machine(Machine_Contient2))==(?);
  List_Instanciated_Parameters(Machine(Interface),Machine(Machine_Possede))==(?);
  List_Instanciated_Parameters(Machine(Interface),Machine(Machine_Emprunt))==(?);
  List_Instanciated_Parameters(Machine(Interface),Machine(Machine_Reservation))==(?);
  List_Instanciated_Parameters(Machine(Interface),Machine(Machine_Client))==(?);
  List_Instanciated_Parameters(Machine(Interface),Machine(Machine_Film))==(?);
  List_Instanciated_Parameters(Machine(Interface),Machine(Machine_Episode))==(?);
  List_Instanciated_Parameters(Machine(Interface),Machine(Machine_Cassette))==(?);
  List_Instanciated_Parameters(Machine(Interface),Machine(Machine_Boutique))==(?);
  List_Instanciated_Parameters(Machine(Interface),Machine(Context))==(?)
END
&
THEORY ListConstraintsX IS
  List_Constraints(Machine(Interface),Machine(Machine_Boutique))==(btrue);
  List_Context_Constraints(Machine(Interface))==(btrue);
  List_Constraints(Machine(Interface))==(btrue);
  List_Constraints(Machine(Interface),Machine(Machine_Cassette))==(btrue);
  List_Constraints(Machine(Interface),Machine(Machine_Episode))==(btrue);
  List_Constraints(Machine(Interface),Machine(Machine_Film))==(btrue);
  List_Constraints(Machine(Interface),Machine(Machine_Client))==(btrue);
  List_Constraints(Machine(Interface),Machine(Machine_Reservation))==(btrue);
  List_Constraints(Machine(Interface),Machine(Machine_Emprunt))==(btrue);
  List_Constraints(Machine(Interface),Machine(Machine_Possede))==(btrue);
  List_Constraints(Machine(Interface),Machine(Machine_Contient2))==(btrue);
  List_Constraints(Machine(Interface),Machine(Machine_Contient1))==(btrue);
  List_Constraints(Machine(Interface),Machine(Machine_Appartient))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Interface))==(creation_client,creation_k7_film,emprunt_film,emprunt_reservation,rendre_K7,resiliation_client);
  List_Operations(Machine(Interface))==(creation_client,creation_k7_film,emprunt_film,emprunt_reservation,rendre_K7,resiliation_client)
END
&
THEORY ListInputX IS
  List_Input(Machine(Interface),creation_client)==(num,nom,adresse,nb_max);
  List_Input(Machine(Interface),creation_k7_film)==(fl,bb,num);
  List_Input(Machine(Interface),emprunt_film)==(cl,fl,bb);
  List_Input(Machine(Interface),emprunt_reservation)==(cl,k7);
  List_Input(Machine(Interface),rendre_K7)==(k7,bb);
  List_Input(Machine(Interface),resiliation_client)==(cl)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Interface),creation_client)==(?);
  List_Output(Machine(Interface),creation_k7_film)==(?);
  List_Output(Machine(Interface),emprunt_film)==(resultat_EF);
  List_Output(Machine(Interface),emprunt_reservation)==(?);
  List_Output(Machine(Interface),rendre_K7)==(?);
  List_Output(Machine(Interface),resiliation_client)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Interface),creation_client)==(creation_client(num,nom,adresse,nb_max));
  List_Header(Machine(Interface),creation_k7_film)==(creation_k7_film(fl,bb,num));
  List_Header(Machine(Interface),emprunt_film)==(resultat_EF <-- emprunt_film(cl,fl,bb));
  List_Header(Machine(Interface),emprunt_reservation)==(emprunt_reservation(cl,k7));
  List_Header(Machine(Interface),rendre_K7)==(rendre_K7(k7,bb));
  List_Header(Machine(Interface),resiliation_client)==(resiliation_client(cl))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Interface),creation_client)==(clients <<: CLIENTS & num: NAT & num/:ran(num_carte) & nom: STRINGS & adresse: STRINGS & nb_max: NAT);
  List_Precondition(Machine(Interface),creation_k7_film)==(cassettes <<: CASSETTES & fl: films & bb: boutiques & num: NAT & num/:ran(num_k7));
  List_Precondition(Machine(Interface),emprunt_film)==(cl: clients & carte_retiree(cl) = FALSE & fl: films & bb: boutiques & appartient~[{bb}]/\contient1~[{fl}]/={} & card(emprunt~[{cl}])+card(reservation~[{cl}])<nombre_max(cl));
  List_Precondition(Machine(Interface),emprunt_reservation)==(cl: clients & carte_retiree(cl) = FALSE & k7: reservation~[{cl}]);
  List_Precondition(Machine(Interface),rendre_K7)==(bb: boutiques & k7: dom(emprunt));
  List_Precondition(Machine(Interface),resiliation_client)==(cl: clients & emprunt~[{cl}] = {})
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Interface),resiliation_client)==(cl: clients & emprunt~[{cl}] = {} & cl: clients | carte_retiree:=carte_retiree<+{cl|->TRUE});
  Expanded_List_Substitution(Machine(Interface),rendre_K7)==(bb: boutiques & k7: dom(emprunt) & k7: dom(emprunt) & bb: boutiques & k7: cassettes | emprunt,date_emp:={k7}<<|emprunt,{k7}<<|date_emp || appartient:=appartient<+{k7|->bb});
  Expanded_List_Substitution(Machine(Interface),emprunt_reservation)==(cl: clients & carte_retiree(cl) = FALSE & k7: reservation~[{cl}] | @date.(date: NAT ==> (k7: dom(reservation) & cl: clients & k7: cassettes-dom(emprunt) & date: NAT & card(emprunt~[{cl}])<nombre_max(cl) | reservation,date_res:={k7}<<|reservation,{k7}<<|date_res || emprunt,date_emp:=emprunt<+{k7|->cl},date_emp<+{k7|->date})));
  Expanded_List_Substitution(Machine(Interface),emprunt_film)==(cl: clients & carte_retiree(cl) = FALSE & fl: films & bb: boutiques & appartient~[{bb}]/\contient1~[{fl}]/={} & card(emprunt~[{cl}])+card(reservation~[{cl}])<nombre_max(cl) | @(k7,date).(k7: appartient~[{bb}]/\contient1~[{fl}] & k7/:dom(emprunt) & k7/:dom(reservation) & date: NAT ==> (cl: clients & k7: cassettes-dom(emprunt) & date: NAT & card(emprunt~[{cl}])<nombre_max(cl) | emprunt,date_emp:=emprunt<+{k7|->cl},date_emp<+{k7|->date} || resultat_EF:=k7)));
  Expanded_List_Substitution(Machine(Interface),creation_k7_film)==(cassettes <<: CASSETTES & fl: films & bb: boutiques & num: NAT & num/:ran(num_k7) | @k7.(k7: CASSETTES-cassettes ==> (k7: CASSETTES-cassettes & num: NAT & fl: films & k7: cassettes & bb: boutiques & k7: cassettes | cassettes,num_k7:=cassettes\/{k7},num_k7<+{k7|->num} || contient1:=contient1<+{k7|->fl} || appartient:=appartient<+{k7|->bb})));
  Expanded_List_Substitution(Machine(Interface),creation_client)==(clients <<: CLIENTS & num: NAT & num/:ran(num_carte) & nom: STRINGS & adresse: STRINGS & nb_max: NAT | @(cl,caution).(cl: CLIENTS-clients & caution: NAT ==> (cl: CLIENTS-clients & num: NAT & nom: STRINGS & adresse: STRINGS & caution: NAT & nb_max: NAT | clients,num_carte,nom_cl,adresse_cl,nombre_max,carte_retiree:=clients\/{cl},num_carte<+{cl|->num},nom_cl<+{cl|->nom},adresse_cl<+{cl|->adresse},nombre_max<+{cl|->nb_max},carte_retiree<+{cl|->FALSE})));
  List_Substitution(Machine(Interface),creation_client)==(ANY cl,caution WHERE cl: CLIENTS-clients & caution: NAT THEN ajout_client(cl,num,nom,adresse,caution,nb_max) END);
  List_Substitution(Machine(Interface),creation_k7_film)==(ANY k7 WHERE k7: CASSETTES-cassettes THEN ajout_cassette(k7,num) || ajout_contient1(fl,k7) || ajout_appartient(k7,bb) END);
  List_Substitution(Machine(Interface),emprunt_film)==(ANY k7,date WHERE k7: appartient~[{bb}]/\contient1~[{fl}] & k7/:dom(emprunt) & k7/:dom(reservation) & date: NAT THEN ajout_emprunt(cl,k7,date) || resultat_EF:=k7 END);
  List_Substitution(Machine(Interface),emprunt_reservation)==(ANY date WHERE date: NAT THEN suppression_reservation(k7) || ajout_emprunt(cl,k7,date) END);
  List_Substitution(Machine(Interface),rendre_K7)==(suppression_emprunt(k7) || ajout_appartient(k7,bb));
  List_Substitution(Machine(Interface),resiliation_client)==(retirer_carte(cl))
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Interface))==(?);
  Inherited_List_Constants(Machine(Interface))==(?);
  List_Constants(Machine(Interface))==(?)
END
&
THEORY ListSetsX IS
  Context_List_Enumerated(Machine(Interface))==(?);
  Context_List_Defered(Machine(Interface))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  Context_List_Sets(Machine(Interface))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  List_Valuable_Sets(Machine(Interface))==(?);
  Inherited_List_Enumerated(Machine(Interface))==(?);
  Inherited_List_Defered(Machine(Interface))==(?);
  Inherited_List_Sets(Machine(Interface))==(?);
  List_Enumerated(Machine(Interface))==(?);
  List_Defered(Machine(Interface))==(?);
  List_Sets(Machine(Interface))==(?)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Interface))==(?);
  Expanded_List_HiddenConstants(Machine(Interface))==(?);
  List_HiddenConstants(Machine(Interface))==(?);
  External_List_HiddenConstants(Machine(Interface))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Interface))==(btrue);
  Context_List_Properties(Machine(Interface))==(STRINGS: FIN(INTEGER) & not(STRINGS = {}) & CLIENTS: FIN(INTEGER) & not(CLIENTS = {}) & BOUTIQUES: FIN(INTEGER) & not(BOUTIQUES = {}) & FILMS: FIN(INTEGER) & not(FILMS = {}) & EPISODES: FIN(INTEGER) & not(EPISODES = {}) & CASSETTES: FIN(INTEGER) & not(CASSETTES = {}));
  Inherited_List_Properties(Machine(Interface))==(btrue);
  List_Properties(Machine(Interface))==(btrue)
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Interface),Machine(Context))==(?);
  Seen_Context_List_Enumerated(Machine(Interface))==(?);
  Seen_Context_List_Invariant(Machine(Interface))==(btrue);
  Seen_Context_List_Assertions(Machine(Interface))==(btrue);
  Seen_Context_List_Properties(Machine(Interface))==(btrue);
  Seen_List_Constraints(Machine(Interface))==(btrue);
  Seen_List_Operations(Machine(Interface),Machine(Context))==(?);
  Seen_Expanded_List_Invariant(Machine(Interface),Machine(Context))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Interface),creation_client)==((Var(cl) == atype(CLIENTS,?,?)),(Var(caution) == btype(INTEGER,?,?)));
  List_ANY_Var(Machine(Interface),creation_k7_film)==(Var(k7) == atype(CASSETTES,?,?));
  List_ANY_Var(Machine(Interface),emprunt_film)==((Var(k7) == atype(CASSETTES,?,?)),(Var(date) == btype(INTEGER,?,?)));
  List_ANY_Var(Machine(Interface),emprunt_reservation)==(Var(date) == btype(INTEGER,?,?));
  List_ANY_Var(Machine(Interface),rendre_K7)==(?);
  List_ANY_Var(Machine(Interface),resiliation_client)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Interface)) == (? | ? | ? | adresse_b,nom_b,boutiques,num_k7,cassettes,titre_ep,num_ep,episodes,genre,titre_fl,num_fl,films,carte_retiree,nombre_max,adresse_cl,nom_cl,num_carte,clients,date_res,reservation,date_emp,emprunt,possede,contient2,contient1,appartient | creation_client,creation_k7_film,emprunt_film,emprunt_reservation,rendre_K7,resiliation_client | ? | seen(Machine(Context)),included(Machine(Machine_Appartient)),included(Machine(Machine_Contient1)),included(Machine(Machine_Contient2)),included(Machine(Machine_Possede)),included(Machine(Machine_Emprunt)),included(Machine(Machine_Reservation)),included(Machine(Machine_Client)),included(Machine(Machine_Film)),included(Machine(Machine_Episode)),included(Machine(Machine_Cassette)),included(Machine(Machine_Boutique)) | ? | Interface);
  List_Of_HiddenCst_Ids(Machine(Interface)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Interface)) == (?);
  List_Of_VisibleVar_Ids(Machine(Interface)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Interface)) == (seen(Machine(Context)): (STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES | ? | ? | ? | ? | ? | ? | ? | ?));
  List_Of_Ids(Machine(Machine_Boutique)) == (? | ? | adresse_b,nom_b,boutiques | ? | ajout_boutique,suppression_boutique | ? | seen(Machine(Context)) | ? | Machine_Boutique);
  List_Of_HiddenCst_Ids(Machine(Machine_Boutique)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Boutique)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Boutique)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Boutique)) == (?: ?);
  List_Of_Ids(Machine(Context)) == (STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES | ? | ? | ? | ? | ? | ? | ? | Context);
  List_Of_HiddenCst_Ids(Machine(Context)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Context)) == (?);
  List_Of_VisibleVar_Ids(Machine(Context)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Context)) == (?: ?);
  List_Of_Ids(Machine(Machine_Cassette)) == (? | ? | num_k7,cassettes | ? | ajout_cassette,suppression_cassette | ? | seen(Machine(Context)) | ? | Machine_Cassette);
  List_Of_HiddenCst_Ids(Machine(Machine_Cassette)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Cassette)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Cassette)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Cassette)) == (?: ?);
  List_Of_Ids(Machine(Machine_Episode)) == (? | ? | titre_ep,num_ep,episodes | ? | ajout_episode,suppression_episode | ? | seen(Machine(Context)) | ? | Machine_Episode);
  List_Of_HiddenCst_Ids(Machine(Machine_Episode)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Episode)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Episode)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Episode)) == (?: ?);
  List_Of_Ids(Machine(Machine_Film)) == (? | ? | genre,titre_fl,num_fl,films | ? | ajout_film,suppression_film | ? | seen(Machine(Context)) | ? | Machine_Film);
  List_Of_HiddenCst_Ids(Machine(Machine_Film)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Film)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Film)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Film)) == (?: ?);
  List_Of_Ids(Machine(Machine_Client)) == (? | ? | carte_retiree,nombre_max,adresse_cl,nom_cl,num_carte,clients | ? | ajout_client,suppression_client,retirer_carte | ? | seen(Machine(Context)) | ? | Machine_Client);
  List_Of_HiddenCst_Ids(Machine(Machine_Client)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Client)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Client)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Client)) == (?: ?);
  List_Of_Ids(Machine(Machine_Reservation)) == (? | ? | date_res,reservation | ? | ajout_reservation,suppression_reservation | ? | seen(Machine(Context)),used(Machine(Machine_Client)),used(Machine(Machine_Cassette)) | ? | Machine_Reservation);
  List_Of_HiddenCst_Ids(Machine(Machine_Reservation)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Reservation)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Reservation)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Reservation)) == (?: ?);
  List_Of_Ids(Machine(Machine_Emprunt)) == (? | ? | date_emp,emprunt | ? | ajout_emprunt,suppression_emprunt | ? | seen(Machine(Context)),used(Machine(Machine_Client)),used(Machine(Machine_Cassette)) | ? | Machine_Emprunt);
  List_Of_HiddenCst_Ids(Machine(Machine_Emprunt)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Emprunt)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Emprunt)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Emprunt)) == (?: ?);
  List_Of_Ids(Machine(Machine_Possede)) == (? | ? | possede | ? | ajout_possede,suppression_possede | ? | seen(Machine(Context)),used(Machine(Machine_Episode)),used(Machine(Machine_Film)) | ? | Machine_Possede);
  List_Of_HiddenCst_Ids(Machine(Machine_Possede)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Possede)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Possede)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Possede)) == (?: ?);
  List_Of_Ids(Machine(Machine_Contient2)) == (? | ? | contient2 | ? | ajout_contient2,suppression_contient2 | ? | seen(Machine(Context)),used(Machine(Machine_Episode)),used(Machine(Machine_Cassette)) | ? | Machine_Contient2);
  List_Of_HiddenCst_Ids(Machine(Machine_Contient2)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Contient2)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Contient2)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Contient2)) == (?: ?);
  List_Of_Ids(Machine(Machine_Contient1)) == (? | ? | contient1 | ? | ajout_contient1,suppression_contient1 | ? | seen(Machine(Context)),used(Machine(Machine_Film)),used(Machine(Machine_Cassette)) | ? | Machine_Contient1);
  List_Of_HiddenCst_Ids(Machine(Machine_Contient1)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Contient1)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Contient1)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Contient1)) == (?: ?);
  List_Of_Ids(Machine(Machine_Appartient)) == (? | ? | appartient | ? | ajout_appartient,suppression_appartient | ? | seen(Machine(Context)),used(Machine(Machine_Boutique)),used(Machine(Machine_Cassette)) | ? | Machine_Appartient);
  List_Of_HiddenCst_Ids(Machine(Machine_Appartient)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Appartient)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Appartient)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Appartient)) == (?: ?)
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Interface)) == (Type(appartient) == Mvl(SetOf(atype(CASSETTES,?,?)*atype(BOUTIQUES,?,?)));Type(contient1) == Mvl(SetOf(atype(CASSETTES,?,?)*atype(FILMS,?,?)));Type(contient2) == Mvl(SetOf(atype(CASSETTES,?,?)*atype(EPISODES,?,?)));Type(possede) == Mvl(SetOf(atype(EPISODES,?,?)*atype(FILMS,?,?)));Type(emprunt) == Mvl(SetOf(atype(CASSETTES,?,?)*atype(CLIENTS,?,?)));Type(date_emp) == Mvl(SetOf(atype(CASSETTES,?,?)*btype(INTEGER,0,MAXINT)));Type(reservation) == Mvl(SetOf(atype(CASSETTES,?,?)*atype(CLIENTS,?,?)));Type(date_res) == Mvl(SetOf(atype(CASSETTES,?,?)*btype(INTEGER,0,MAXINT)));Type(clients) == Mvl(SetOf(atype(CLIENTS,?,?)));Type(num_carte) == Mvl(SetOf(atype(CLIENTS,?,?)*btype(INTEGER,0,MAXINT)));Type(nom_cl) == Mvl(SetOf(atype(CLIENTS,?,?)*atype(STRINGS,"[STRINGS","]STRINGS")));Type(adresse_cl) == Mvl(SetOf(atype(CLIENTS,?,?)*atype(STRINGS,"[STRINGS","]STRINGS")));Type(nombre_max) == Mvl(SetOf(atype(CLIENTS,?,?)*btype(INTEGER,0,MAXINT)));Type(carte_retiree) == Mvl(SetOf(atype(CLIENTS,?,?)*btype(BOOL,0,1)));Type(films) == Mvl(SetOf(atype(FILMS,?,?)));Type(num_fl) == Mvl(SetOf(atype(FILMS,?,?)*btype(INTEGER,0,MAXINT)));Type(titre_fl) == Mvl(SetOf(atype(FILMS,?,?)*atype(STRINGS,"[STRINGS","]STRINGS")));Type(genre) == Mvl(SetOf(atype(FILMS,?,?)*atype(STRINGS,"[STRINGS","]STRINGS")));Type(episodes) == Mvl(SetOf(atype(EPISODES,?,?)));Type(num_ep) == Mvl(SetOf(atype(EPISODES,?,?)*btype(INTEGER,0,MAXINT)));Type(titre_ep) == Mvl(SetOf(atype(EPISODES,?,?)*atype(STRINGS,"[STRINGS","]STRINGS")));Type(cassettes) == Mvl(SetOf(atype(CASSETTES,?,?)));Type(num_k7) == Mvl(SetOf(atype(CASSETTES,?,?)*btype(INTEGER,0,MAXINT)));Type(boutiques) == Mvl(SetOf(atype(BOUTIQUES,?,?)));Type(nom_b) == Mvl(SetOf(atype(BOUTIQUES,?,?)*atype(STRINGS,"[STRINGS","]STRINGS")));Type(adresse_b) == Mvl(SetOf(atype(BOUTIQUES,?,?)*atype(STRINGS,"[STRINGS","]STRINGS"))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Interface)) == (Type(resiliation_client) == Cst(No_type,atype(CLIENTS,?,?));Type(rendre_K7) == Cst(No_type,atype(CASSETTES,?,?)*atype(BOUTIQUES,?,?));Type(emprunt_reservation) == Cst(No_type,atype(CLIENTS,?,?)*atype(CASSETTES,?,?));Type(emprunt_film) == Cst(atype(CASSETTES,?,?),atype(CLIENTS,?,?)*atype(FILMS,?,?)*atype(BOUTIQUES,?,?));Type(creation_k7_film) == Cst(No_type,atype(FILMS,?,?)*atype(BOUTIQUES,?,?)*btype(INTEGER,?,?));Type(creation_client) == Cst(No_type,btype(INTEGER,?,?)*atype(STRINGS,?,?)*atype(STRINGS,?,?)*btype(INTEGER,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
