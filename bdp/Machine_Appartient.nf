﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Machine_Appartient))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Machine_Appartient))==(Machine(Machine_Appartient));
  Level(Machine(Machine_Appartient))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Machine_Appartient)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Machine_Appartient))==(Context)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Machine_Appartient))==(Machine_Boutique,Machine_Cassette)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Machine_Appartient))==(?);
  List_Includes(Machine(Machine_Appartient))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Machine_Appartient))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Machine_Appartient))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Machine_Appartient))==(adresse_b,nom_b,boutiques,num_k7,cassettes);
  Context_List_Variables(Machine(Machine_Appartient))==(adresse_b,nom_b,boutiques,num_k7,cassettes);
  Abstract_List_Variables(Machine(Machine_Appartient))==(?);
  Local_List_Variables(Machine(Machine_Appartient))==(appartient);
  List_Variables(Machine(Machine_Appartient))==(appartient);
  External_List_Variables(Machine(Machine_Appartient))==(appartient)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Machine_Appartient))==(?);
  Abstract_List_VisibleVariables(Machine(Machine_Appartient))==(?);
  External_List_VisibleVariables(Machine(Machine_Appartient))==(?);
  Expanded_List_VisibleVariables(Machine(Machine_Appartient))==(?);
  List_VisibleVariables(Machine(Machine_Appartient))==(?);
  Internal_List_VisibleVariables(Machine(Machine_Appartient))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Machine_Appartient))==(btrue);
  Gluing_List_Invariant(Machine(Machine_Appartient))==(appartient: cassettes --> boutiques);
  Expanded_List_Invariant(Machine(Machine_Appartient))==(btrue);
  Abstract_List_Invariant(Machine(Machine_Appartient))==(btrue);
  Context_List_Invariant(Machine(Machine_Appartient))==(boutiques <: BOUTIQUES & nom_b: boutiques --> STRINGS & adresse_b: boutiques --> STRINGS & cassettes <: CASSETTES & num_k7: cassettes --> NAT);
  List_Invariant(Machine(Machine_Appartient))==(btrue)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Machine_Appartient))==(btrue);
  Abstract_List_Assertions(Machine(Machine_Appartient))==(btrue);
  Context_List_Assertions(Machine(Machine_Appartient))==(btrue);
  List_Assertions(Machine(Machine_Appartient))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Machine_Appartient))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Machine_Appartient))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Machine_Appartient))==(appartient:={});
  Context_List_Initialisation(Machine(Machine_Appartient))==(boutiques,nom_b,adresse_b:={},{},{};cassettes,num_k7:={},{});
  List_Initialisation(Machine(Machine_Appartient))==(appartient:={})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Machine_Appartient))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Machine_Appartient),Machine(Machine_Boutique))==(?);
  List_Instanciated_Parameters(Machine(Machine_Appartient),Machine(Machine_Cassette))==(?);
  List_Instanciated_Parameters(Machine(Machine_Appartient),Machine(Context))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Machine_Appartient))==(btrue);
  List_Constraints(Machine(Machine_Appartient))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Machine_Appartient))==(ajout_appartient,suppression_appartient);
  List_Operations(Machine(Machine_Appartient))==(ajout_appartient,suppression_appartient)
END
&
THEORY ListInputX IS
  List_Input(Machine(Machine_Appartient),ajout_appartient)==(k7,bt);
  List_Input(Machine(Machine_Appartient),suppression_appartient)==(k7)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Machine_Appartient),ajout_appartient)==(?);
  List_Output(Machine(Machine_Appartient),suppression_appartient)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Machine_Appartient),ajout_appartient)==(ajout_appartient(k7,bt));
  List_Header(Machine(Machine_Appartient),suppression_appartient)==(suppression_appartient(k7))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Machine_Appartient),ajout_appartient)==(bt: boutiques & k7: cassettes);
  List_Precondition(Machine(Machine_Appartient),suppression_appartient)==(k7: dom(appartient))
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Machine_Appartient),suppression_appartient)==(k7: dom(appartient) | appartient:={k7}<<|appartient);
  Expanded_List_Substitution(Machine(Machine_Appartient),ajout_appartient)==(bt: boutiques & k7: cassettes | appartient:=appartient<+{k7|->bt});
  List_Substitution(Machine(Machine_Appartient),ajout_appartient)==(appartient(k7):=bt);
  List_Substitution(Machine(Machine_Appartient),suppression_appartient)==(appartient:={k7}<<|appartient)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Machine_Appartient))==(?);
  Inherited_List_Constants(Machine(Machine_Appartient))==(?);
  List_Constants(Machine(Machine_Appartient))==(?)
END
&
THEORY ListSetsX IS
  Context_List_Enumerated(Machine(Machine_Appartient))==(?);
  Context_List_Defered(Machine(Machine_Appartient))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  Context_List_Sets(Machine(Machine_Appartient))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  List_Valuable_Sets(Machine(Machine_Appartient))==(?);
  Inherited_List_Enumerated(Machine(Machine_Appartient))==(?);
  Inherited_List_Defered(Machine(Machine_Appartient))==(?);
  Inherited_List_Sets(Machine(Machine_Appartient))==(?);
  List_Enumerated(Machine(Machine_Appartient))==(?);
  List_Defered(Machine(Machine_Appartient))==(?);
  List_Sets(Machine(Machine_Appartient))==(?)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Machine_Appartient))==(?);
  Expanded_List_HiddenConstants(Machine(Machine_Appartient))==(?);
  List_HiddenConstants(Machine(Machine_Appartient))==(?);
  External_List_HiddenConstants(Machine(Machine_Appartient))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Machine_Appartient))==(btrue);
  Context_List_Properties(Machine(Machine_Appartient))==(STRINGS: FIN(INTEGER) & not(STRINGS = {}) & CLIENTS: FIN(INTEGER) & not(CLIENTS = {}) & BOUTIQUES: FIN(INTEGER) & not(BOUTIQUES = {}) & FILMS: FIN(INTEGER) & not(FILMS = {}) & EPISODES: FIN(INTEGER) & not(EPISODES = {}) & CASSETTES: FIN(INTEGER) & not(CASSETTES = {}));
  Inherited_List_Properties(Machine(Machine_Appartient))==(btrue);
  List_Properties(Machine(Machine_Appartient))==(btrue)
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Machine_Appartient),Machine(Context))==(?);
  Seen_Context_List_Enumerated(Machine(Machine_Appartient))==(?);
  Seen_Context_List_Invariant(Machine(Machine_Appartient))==(btrue);
  Seen_Context_List_Assertions(Machine(Machine_Appartient))==(btrue);
  Seen_Context_List_Properties(Machine(Machine_Appartient))==(btrue);
  Seen_List_Constraints(Machine(Machine_Appartient))==(btrue);
  Seen_List_Operations(Machine(Machine_Appartient),Machine(Context))==(?);
  Seen_Expanded_List_Invariant(Machine(Machine_Appartient),Machine(Context))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Machine_Appartient),ajout_appartient)==(?);
  List_ANY_Var(Machine(Machine_Appartient),suppression_appartient)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Machine_Appartient)) == (? | ? | appartient | ? | ajout_appartient,suppression_appartient | ? | seen(Machine(Context)),used(Machine(Machine_Boutique)),used(Machine(Machine_Cassette)) | ? | Machine_Appartient);
  List_Of_HiddenCst_Ids(Machine(Machine_Appartient)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Appartient)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Appartient)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Appartient)) == (?: ?);
  List_Of_Ids(Machine(Machine_Cassette)) == (? | ? | num_k7,cassettes | ? | ajout_cassette,suppression_cassette | ? | seen(Machine(Context)) | ? | Machine_Cassette);
  List_Of_HiddenCst_Ids(Machine(Machine_Cassette)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Cassette)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Cassette)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Cassette)) == (?: ?);
  List_Of_Ids(Machine(Context)) == (STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES | ? | ? | ? | ? | ? | ? | ? | Context);
  List_Of_HiddenCst_Ids(Machine(Context)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Context)) == (?);
  List_Of_VisibleVar_Ids(Machine(Context)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Context)) == (?: ?);
  List_Of_Ids(Machine(Machine_Boutique)) == (? | ? | adresse_b,nom_b,boutiques | ? | ajout_boutique,suppression_boutique | ? | seen(Machine(Context)) | ? | Machine_Boutique);
  List_Of_HiddenCst_Ids(Machine(Machine_Boutique)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Boutique)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Boutique)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Boutique)) == (?: ?)
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Machine_Appartient)) == (Type(appartient) == Mvl(SetOf(atype(CASSETTES,?,?)*atype(BOUTIQUES,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Machine_Appartient)) == (Type(suppression_appartient) == Cst(No_type,atype(CASSETTES,?,?));Type(ajout_appartient) == Cst(No_type,atype(CASSETTES,?,?)*atype(BOUTIQUES,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
