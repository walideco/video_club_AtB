﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Machine_Cassette))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Machine_Cassette))==(Machine(Machine_Cassette));
  Level(Machine(Machine_Cassette))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Machine_Cassette)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Machine_Cassette))==(Context)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Machine_Cassette))==(?)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Machine_Cassette))==(?);
  List_Includes(Machine(Machine_Cassette))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Machine_Cassette))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Machine_Cassette))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Machine_Cassette))==(?);
  Context_List_Variables(Machine(Machine_Cassette))==(?);
  Abstract_List_Variables(Machine(Machine_Cassette))==(?);
  Local_List_Variables(Machine(Machine_Cassette))==(num_k7,cassettes);
  List_Variables(Machine(Machine_Cassette))==(num_k7,cassettes);
  External_List_Variables(Machine(Machine_Cassette))==(num_k7,cassettes)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Machine_Cassette))==(?);
  Abstract_List_VisibleVariables(Machine(Machine_Cassette))==(?);
  External_List_VisibleVariables(Machine(Machine_Cassette))==(?);
  Expanded_List_VisibleVariables(Machine(Machine_Cassette))==(?);
  List_VisibleVariables(Machine(Machine_Cassette))==(?);
  Internal_List_VisibleVariables(Machine(Machine_Cassette))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Machine_Cassette))==(btrue);
  Gluing_List_Invariant(Machine(Machine_Cassette))==(btrue);
  Expanded_List_Invariant(Machine(Machine_Cassette))==(btrue);
  Abstract_List_Invariant(Machine(Machine_Cassette))==(btrue);
  Context_List_Invariant(Machine(Machine_Cassette))==(btrue);
  List_Invariant(Machine(Machine_Cassette))==(cassettes <: CASSETTES & num_k7: cassettes --> NAT)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Machine_Cassette))==(btrue);
  Abstract_List_Assertions(Machine(Machine_Cassette))==(btrue);
  Context_List_Assertions(Machine(Machine_Cassette))==(btrue);
  List_Assertions(Machine(Machine_Cassette))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Machine_Cassette))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Machine_Cassette))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Machine_Cassette))==(cassettes,num_k7:={},{});
  Context_List_Initialisation(Machine(Machine_Cassette))==(skip);
  List_Initialisation(Machine(Machine_Cassette))==(cassettes,num_k7:={},{})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Machine_Cassette))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Machine_Cassette),Machine(Context))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Machine_Cassette))==(btrue);
  List_Constraints(Machine(Machine_Cassette))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Machine_Cassette))==(ajout_cassette,suppression_cassette);
  List_Operations(Machine(Machine_Cassette))==(ajout_cassette,suppression_cassette)
END
&
THEORY ListInputX IS
  List_Input(Machine(Machine_Cassette),ajout_cassette)==(cs,num);
  List_Input(Machine(Machine_Cassette),suppression_cassette)==(cs)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Machine_Cassette),ajout_cassette)==(?);
  List_Output(Machine(Machine_Cassette),suppression_cassette)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Machine_Cassette),ajout_cassette)==(ajout_cassette(cs,num));
  List_Header(Machine(Machine_Cassette),suppression_cassette)==(suppression_cassette(cs))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Machine_Cassette),ajout_cassette)==(cs: CASSETTES-cassettes & num: NAT);
  List_Precondition(Machine(Machine_Cassette),suppression_cassette)==(cs: cassettes)
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Machine_Cassette),suppression_cassette)==(cs: cassettes | cassettes,num_k7:=cassettes-{cs},{cs}<<|num_k7);
  Expanded_List_Substitution(Machine(Machine_Cassette),ajout_cassette)==(cs: CASSETTES-cassettes & num: NAT | cassettes,num_k7:=cassettes\/{cs},num_k7<+{cs|->num});
  List_Substitution(Machine(Machine_Cassette),ajout_cassette)==(cassettes:=cassettes\/{cs} || num_k7(cs):=num);
  List_Substitution(Machine(Machine_Cassette),suppression_cassette)==(cassettes:=cassettes-{cs} || num_k7:={cs}<<|num_k7)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Machine_Cassette))==(?);
  Inherited_List_Constants(Machine(Machine_Cassette))==(?);
  List_Constants(Machine(Machine_Cassette))==(?)
END
&
THEORY ListSetsX IS
  Context_List_Enumerated(Machine(Machine_Cassette))==(?);
  Context_List_Defered(Machine(Machine_Cassette))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  Context_List_Sets(Machine(Machine_Cassette))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  List_Valuable_Sets(Machine(Machine_Cassette))==(?);
  Inherited_List_Enumerated(Machine(Machine_Cassette))==(?);
  Inherited_List_Defered(Machine(Machine_Cassette))==(?);
  Inherited_List_Sets(Machine(Machine_Cassette))==(?);
  List_Enumerated(Machine(Machine_Cassette))==(?);
  List_Defered(Machine(Machine_Cassette))==(?);
  List_Sets(Machine(Machine_Cassette))==(?)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Machine_Cassette))==(?);
  Expanded_List_HiddenConstants(Machine(Machine_Cassette))==(?);
  List_HiddenConstants(Machine(Machine_Cassette))==(?);
  External_List_HiddenConstants(Machine(Machine_Cassette))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Machine_Cassette))==(btrue);
  Context_List_Properties(Machine(Machine_Cassette))==(STRINGS: FIN(INTEGER) & not(STRINGS = {}) & CLIENTS: FIN(INTEGER) & not(CLIENTS = {}) & BOUTIQUES: FIN(INTEGER) & not(BOUTIQUES = {}) & FILMS: FIN(INTEGER) & not(FILMS = {}) & EPISODES: FIN(INTEGER) & not(EPISODES = {}) & CASSETTES: FIN(INTEGER) & not(CASSETTES = {}));
  Inherited_List_Properties(Machine(Machine_Cassette))==(btrue);
  List_Properties(Machine(Machine_Cassette))==(btrue)
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Machine_Cassette),Machine(Context))==(?);
  Seen_Context_List_Enumerated(Machine(Machine_Cassette))==(?);
  Seen_Context_List_Invariant(Machine(Machine_Cassette))==(btrue);
  Seen_Context_List_Assertions(Machine(Machine_Cassette))==(btrue);
  Seen_Context_List_Properties(Machine(Machine_Cassette))==(btrue);
  Seen_List_Constraints(Machine(Machine_Cassette))==(btrue);
  Seen_List_Operations(Machine(Machine_Cassette),Machine(Context))==(?);
  Seen_Expanded_List_Invariant(Machine(Machine_Cassette),Machine(Context))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Machine_Cassette),ajout_cassette)==(?);
  List_ANY_Var(Machine(Machine_Cassette),suppression_cassette)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Machine_Cassette)) == (? | ? | num_k7,cassettes | ? | ajout_cassette,suppression_cassette | ? | seen(Machine(Context)) | ? | Machine_Cassette);
  List_Of_HiddenCst_Ids(Machine(Machine_Cassette)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Cassette)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Cassette)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Cassette)) == (?: ?);
  List_Of_Ids(Machine(Context)) == (STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES | ? | ? | ? | ? | ? | ? | ? | Context);
  List_Of_HiddenCst_Ids(Machine(Context)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Context)) == (?);
  List_Of_VisibleVar_Ids(Machine(Context)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Context)) == (?: ?)
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Machine_Cassette)) == (Type(num_k7) == Mvl(SetOf(atype(CASSETTES,?,?)*btype(INTEGER,0,MAXINT)));Type(cassettes) == Mvl(SetOf(atype(CASSETTES,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Machine_Cassette)) == (Type(suppression_cassette) == Cst(No_type,atype(CASSETTES,?,?));Type(ajout_cassette) == Cst(No_type,atype(CASSETTES,?,?)*btype(INTEGER,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
