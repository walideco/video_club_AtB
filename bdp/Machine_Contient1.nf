﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Machine_Contient1))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Machine_Contient1))==(Machine(Machine_Contient1));
  Level(Machine(Machine_Contient1))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Machine_Contient1)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Machine_Contient1))==(Context)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Machine_Contient1))==(Machine_Film,Machine_Cassette)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Machine_Contient1))==(?);
  List_Includes(Machine(Machine_Contient1))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Machine_Contient1))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Machine_Contient1))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Machine_Contient1))==(genre,titre_fl,num_fl,films,num_k7,cassettes);
  Context_List_Variables(Machine(Machine_Contient1))==(genre,titre_fl,num_fl,films,num_k7,cassettes);
  Abstract_List_Variables(Machine(Machine_Contient1))==(?);
  Local_List_Variables(Machine(Machine_Contient1))==(contient1);
  List_Variables(Machine(Machine_Contient1))==(contient1);
  External_List_Variables(Machine(Machine_Contient1))==(contient1)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Machine_Contient1))==(?);
  Abstract_List_VisibleVariables(Machine(Machine_Contient1))==(?);
  External_List_VisibleVariables(Machine(Machine_Contient1))==(?);
  Expanded_List_VisibleVariables(Machine(Machine_Contient1))==(?);
  List_VisibleVariables(Machine(Machine_Contient1))==(?);
  Internal_List_VisibleVariables(Machine(Machine_Contient1))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Machine_Contient1))==(btrue);
  Gluing_List_Invariant(Machine(Machine_Contient1))==(contient1: cassettes --> films);
  Expanded_List_Invariant(Machine(Machine_Contient1))==(btrue);
  Abstract_List_Invariant(Machine(Machine_Contient1))==(btrue);
  Context_List_Invariant(Machine(Machine_Contient1))==(films <: FILMS & num_fl: films --> NAT & titre_fl: films --> STRINGS & genre: films --> STRINGS & cassettes <: CASSETTES & num_k7: cassettes --> NAT);
  List_Invariant(Machine(Machine_Contient1))==(btrue)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Machine_Contient1))==(btrue);
  Abstract_List_Assertions(Machine(Machine_Contient1))==(btrue);
  Context_List_Assertions(Machine(Machine_Contient1))==(btrue);
  List_Assertions(Machine(Machine_Contient1))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Machine_Contient1))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Machine_Contient1))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Machine_Contient1))==(contient1:={});
  Context_List_Initialisation(Machine(Machine_Contient1))==(films,num_fl,titre_fl,genre:={},{},{},{};cassettes,num_k7:={},{});
  List_Initialisation(Machine(Machine_Contient1))==(contient1:={})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Machine_Contient1))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Machine_Contient1),Machine(Machine_Film))==(?);
  List_Instanciated_Parameters(Machine(Machine_Contient1),Machine(Machine_Cassette))==(?);
  List_Instanciated_Parameters(Machine(Machine_Contient1),Machine(Context))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Machine_Contient1))==(btrue);
  List_Constraints(Machine(Machine_Contient1))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Machine_Contient1))==(ajout_contient1,suppression_contient1);
  List_Operations(Machine(Machine_Contient1))==(ajout_contient1,suppression_contient1)
END
&
THEORY ListInputX IS
  List_Input(Machine(Machine_Contient1),ajout_contient1)==(fl,k7);
  List_Input(Machine(Machine_Contient1),suppression_contient1)==(k7)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Machine_Contient1),ajout_contient1)==(?);
  List_Output(Machine(Machine_Contient1),suppression_contient1)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Machine_Contient1),ajout_contient1)==(ajout_contient1(fl,k7));
  List_Header(Machine(Machine_Contient1),suppression_contient1)==(suppression_contient1(k7))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Machine_Contient1),ajout_contient1)==(fl: films & k7: cassettes);
  List_Precondition(Machine(Machine_Contient1),suppression_contient1)==(k7: dom(contient1))
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Machine_Contient1),suppression_contient1)==(k7: dom(contient1) | contient1:={k7}<<|contient1);
  Expanded_List_Substitution(Machine(Machine_Contient1),ajout_contient1)==(fl: films & k7: cassettes | contient1:=contient1<+{k7|->fl});
  List_Substitution(Machine(Machine_Contient1),ajout_contient1)==(contient1(k7):=fl);
  List_Substitution(Machine(Machine_Contient1),suppression_contient1)==(contient1:={k7}<<|contient1)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Machine_Contient1))==(?);
  Inherited_List_Constants(Machine(Machine_Contient1))==(?);
  List_Constants(Machine(Machine_Contient1))==(?)
END
&
THEORY ListSetsX IS
  Context_List_Enumerated(Machine(Machine_Contient1))==(?);
  Context_List_Defered(Machine(Machine_Contient1))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  Context_List_Sets(Machine(Machine_Contient1))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  List_Valuable_Sets(Machine(Machine_Contient1))==(?);
  Inherited_List_Enumerated(Machine(Machine_Contient1))==(?);
  Inherited_List_Defered(Machine(Machine_Contient1))==(?);
  Inherited_List_Sets(Machine(Machine_Contient1))==(?);
  List_Enumerated(Machine(Machine_Contient1))==(?);
  List_Defered(Machine(Machine_Contient1))==(?);
  List_Sets(Machine(Machine_Contient1))==(?)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Machine_Contient1))==(?);
  Expanded_List_HiddenConstants(Machine(Machine_Contient1))==(?);
  List_HiddenConstants(Machine(Machine_Contient1))==(?);
  External_List_HiddenConstants(Machine(Machine_Contient1))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Machine_Contient1))==(btrue);
  Context_List_Properties(Machine(Machine_Contient1))==(STRINGS: FIN(INTEGER) & not(STRINGS = {}) & CLIENTS: FIN(INTEGER) & not(CLIENTS = {}) & BOUTIQUES: FIN(INTEGER) & not(BOUTIQUES = {}) & FILMS: FIN(INTEGER) & not(FILMS = {}) & EPISODES: FIN(INTEGER) & not(EPISODES = {}) & CASSETTES: FIN(INTEGER) & not(CASSETTES = {}));
  Inherited_List_Properties(Machine(Machine_Contient1))==(btrue);
  List_Properties(Machine(Machine_Contient1))==(btrue)
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Machine_Contient1),Machine(Context))==(?);
  Seen_Context_List_Enumerated(Machine(Machine_Contient1))==(?);
  Seen_Context_List_Invariant(Machine(Machine_Contient1))==(btrue);
  Seen_Context_List_Assertions(Machine(Machine_Contient1))==(btrue);
  Seen_Context_List_Properties(Machine(Machine_Contient1))==(btrue);
  Seen_List_Constraints(Machine(Machine_Contient1))==(btrue);
  Seen_List_Operations(Machine(Machine_Contient1),Machine(Context))==(?);
  Seen_Expanded_List_Invariant(Machine(Machine_Contient1),Machine(Context))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Machine_Contient1),ajout_contient1)==(?);
  List_ANY_Var(Machine(Machine_Contient1),suppression_contient1)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Machine_Contient1)) == (? | ? | contient1 | ? | ajout_contient1,suppression_contient1 | ? | seen(Machine(Context)),used(Machine(Machine_Film)),used(Machine(Machine_Cassette)) | ? | Machine_Contient1);
  List_Of_HiddenCst_Ids(Machine(Machine_Contient1)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Contient1)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Contient1)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Contient1)) == (?: ?);
  List_Of_Ids(Machine(Machine_Cassette)) == (? | ? | num_k7,cassettes | ? | ajout_cassette,suppression_cassette | ? | seen(Machine(Context)) | ? | Machine_Cassette);
  List_Of_HiddenCst_Ids(Machine(Machine_Cassette)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Cassette)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Cassette)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Cassette)) == (?: ?);
  List_Of_Ids(Machine(Context)) == (STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES | ? | ? | ? | ? | ? | ? | ? | Context);
  List_Of_HiddenCst_Ids(Machine(Context)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Context)) == (?);
  List_Of_VisibleVar_Ids(Machine(Context)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Context)) == (?: ?);
  List_Of_Ids(Machine(Machine_Film)) == (? | ? | genre,titre_fl,num_fl,films | ? | ajout_film,suppression_film | ? | seen(Machine(Context)) | ? | Machine_Film);
  List_Of_HiddenCst_Ids(Machine(Machine_Film)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Film)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Film)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Film)) == (?: ?)
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Machine_Contient1)) == (Type(contient1) == Mvl(SetOf(atype(CASSETTES,?,?)*atype(FILMS,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Machine_Contient1)) == (Type(suppression_contient1) == Cst(No_type,atype(CASSETTES,?,?));Type(ajout_contient1) == Cst(No_type,atype(FILMS,?,?)*atype(CASSETTES,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
