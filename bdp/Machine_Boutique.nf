﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Machine_Boutique))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Machine_Boutique))==(Machine(Machine_Boutique));
  Level(Machine(Machine_Boutique))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Machine_Boutique)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Machine_Boutique))==(Context)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Machine_Boutique))==(?)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Machine_Boutique))==(?);
  List_Includes(Machine(Machine_Boutique))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Machine_Boutique))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Machine_Boutique))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Machine_Boutique))==(?);
  Context_List_Variables(Machine(Machine_Boutique))==(?);
  Abstract_List_Variables(Machine(Machine_Boutique))==(?);
  Local_List_Variables(Machine(Machine_Boutique))==(adresse_b,nom_b,boutiques);
  List_Variables(Machine(Machine_Boutique))==(adresse_b,nom_b,boutiques);
  External_List_Variables(Machine(Machine_Boutique))==(adresse_b,nom_b,boutiques)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Machine_Boutique))==(?);
  Abstract_List_VisibleVariables(Machine(Machine_Boutique))==(?);
  External_List_VisibleVariables(Machine(Machine_Boutique))==(?);
  Expanded_List_VisibleVariables(Machine(Machine_Boutique))==(?);
  List_VisibleVariables(Machine(Machine_Boutique))==(?);
  Internal_List_VisibleVariables(Machine(Machine_Boutique))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Machine_Boutique))==(btrue);
  Gluing_List_Invariant(Machine(Machine_Boutique))==(btrue);
  Expanded_List_Invariant(Machine(Machine_Boutique))==(btrue);
  Abstract_List_Invariant(Machine(Machine_Boutique))==(btrue);
  Context_List_Invariant(Machine(Machine_Boutique))==(btrue);
  List_Invariant(Machine(Machine_Boutique))==(boutiques <: BOUTIQUES & nom_b: boutiques --> STRINGS & adresse_b: boutiques --> STRINGS)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Machine_Boutique))==(btrue);
  Abstract_List_Assertions(Machine(Machine_Boutique))==(btrue);
  Context_List_Assertions(Machine(Machine_Boutique))==(btrue);
  List_Assertions(Machine(Machine_Boutique))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Machine_Boutique))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Machine_Boutique))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Machine_Boutique))==(boutiques,nom_b,adresse_b:={},{},{});
  Context_List_Initialisation(Machine(Machine_Boutique))==(skip);
  List_Initialisation(Machine(Machine_Boutique))==(boutiques,nom_b,adresse_b:={},{},{})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Machine_Boutique))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Machine_Boutique),Machine(Context))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Machine_Boutique))==(btrue);
  List_Constraints(Machine(Machine_Boutique))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Machine_Boutique))==(ajout_boutique,suppression_boutique);
  List_Operations(Machine(Machine_Boutique))==(ajout_boutique,suppression_boutique)
END
&
THEORY ListInputX IS
  List_Input(Machine(Machine_Boutique),ajout_boutique)==(bb,nom,adresse);
  List_Input(Machine(Machine_Boutique),suppression_boutique)==(bb)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Machine_Boutique),ajout_boutique)==(?);
  List_Output(Machine(Machine_Boutique),suppression_boutique)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Machine_Boutique),ajout_boutique)==(ajout_boutique(bb,nom,adresse));
  List_Header(Machine(Machine_Boutique),suppression_boutique)==(suppression_boutique(bb))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Machine_Boutique),ajout_boutique)==(bb: BOUTIQUES-boutiques & nom: STRINGS & adresse: STRINGS);
  List_Precondition(Machine(Machine_Boutique),suppression_boutique)==(bb: boutiques)
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Machine_Boutique),suppression_boutique)==(bb: boutiques | boutiques,nom_b,adresse_b:=boutiques-{bb},{bb}<<|nom_b,{bb}<<|adresse_b);
  Expanded_List_Substitution(Machine(Machine_Boutique),ajout_boutique)==(bb: BOUTIQUES-boutiques & nom: STRINGS & adresse: STRINGS | boutiques,nom_b,adresse_b:=boutiques\/{bb},nom_b<+{bb|->nom},adresse_b<+{bb|->adresse});
  List_Substitution(Machine(Machine_Boutique),ajout_boutique)==(boutiques:=boutiques\/{bb} || nom_b(bb):=nom || adresse_b(bb):=adresse);
  List_Substitution(Machine(Machine_Boutique),suppression_boutique)==(boutiques:=boutiques-{bb} || nom_b:={bb}<<|nom_b || adresse_b:={bb}<<|adresse_b)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Machine_Boutique))==(?);
  Inherited_List_Constants(Machine(Machine_Boutique))==(?);
  List_Constants(Machine(Machine_Boutique))==(?)
END
&
THEORY ListSetsX IS
  Context_List_Enumerated(Machine(Machine_Boutique))==(?);
  Context_List_Defered(Machine(Machine_Boutique))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  Context_List_Sets(Machine(Machine_Boutique))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  List_Valuable_Sets(Machine(Machine_Boutique))==(?);
  Inherited_List_Enumerated(Machine(Machine_Boutique))==(?);
  Inherited_List_Defered(Machine(Machine_Boutique))==(?);
  Inherited_List_Sets(Machine(Machine_Boutique))==(?);
  List_Enumerated(Machine(Machine_Boutique))==(?);
  List_Defered(Machine(Machine_Boutique))==(?);
  List_Sets(Machine(Machine_Boutique))==(?)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Machine_Boutique))==(?);
  Expanded_List_HiddenConstants(Machine(Machine_Boutique))==(?);
  List_HiddenConstants(Machine(Machine_Boutique))==(?);
  External_List_HiddenConstants(Machine(Machine_Boutique))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Machine_Boutique))==(btrue);
  Context_List_Properties(Machine(Machine_Boutique))==(STRINGS: FIN(INTEGER) & not(STRINGS = {}) & CLIENTS: FIN(INTEGER) & not(CLIENTS = {}) & BOUTIQUES: FIN(INTEGER) & not(BOUTIQUES = {}) & FILMS: FIN(INTEGER) & not(FILMS = {}) & EPISODES: FIN(INTEGER) & not(EPISODES = {}) & CASSETTES: FIN(INTEGER) & not(CASSETTES = {}));
  Inherited_List_Properties(Machine(Machine_Boutique))==(btrue);
  List_Properties(Machine(Machine_Boutique))==(btrue)
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Machine_Boutique),Machine(Context))==(?);
  Seen_Context_List_Enumerated(Machine(Machine_Boutique))==(?);
  Seen_Context_List_Invariant(Machine(Machine_Boutique))==(btrue);
  Seen_Context_List_Assertions(Machine(Machine_Boutique))==(btrue);
  Seen_Context_List_Properties(Machine(Machine_Boutique))==(btrue);
  Seen_List_Constraints(Machine(Machine_Boutique))==(btrue);
  Seen_List_Operations(Machine(Machine_Boutique),Machine(Context))==(?);
  Seen_Expanded_List_Invariant(Machine(Machine_Boutique),Machine(Context))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Machine_Boutique),ajout_boutique)==(?);
  List_ANY_Var(Machine(Machine_Boutique),suppression_boutique)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Machine_Boutique)) == (? | ? | adresse_b,nom_b,boutiques | ? | ajout_boutique,suppression_boutique | ? | seen(Machine(Context)) | ? | Machine_Boutique);
  List_Of_HiddenCst_Ids(Machine(Machine_Boutique)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Boutique)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Boutique)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Boutique)) == (?: ?);
  List_Of_Ids(Machine(Context)) == (STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES | ? | ? | ? | ? | ? | ? | ? | Context);
  List_Of_HiddenCst_Ids(Machine(Context)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Context)) == (?);
  List_Of_VisibleVar_Ids(Machine(Context)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Context)) == (?: ?)
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Machine_Boutique)) == (Type(adresse_b) == Mvl(SetOf(atype(BOUTIQUES,?,?)*atype(STRINGS,"[STRINGS","]STRINGS")));Type(nom_b) == Mvl(SetOf(atype(BOUTIQUES,?,?)*atype(STRINGS,"[STRINGS","]STRINGS")));Type(boutiques) == Mvl(SetOf(atype(BOUTIQUES,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Machine_Boutique)) == (Type(suppression_boutique) == Cst(No_type,atype(BOUTIQUES,?,?));Type(ajout_boutique) == Cst(No_type,atype(BOUTIQUES,?,?)*atype(STRINGS,?,?)*atype(STRINGS,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
