﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Machine_Reservation))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Machine_Reservation))==(Machine(Machine_Reservation));
  Level(Machine(Machine_Reservation))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Machine_Reservation)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Machine_Reservation))==(Context)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Machine_Reservation))==(Machine_Client,Machine_Cassette)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Machine_Reservation))==(?);
  List_Includes(Machine(Machine_Reservation))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Machine_Reservation))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Machine_Reservation))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Machine_Reservation))==(carte_retiree,nombre_max,adresse_cl,nom_cl,num_carte,clients,num_k7,cassettes);
  Context_List_Variables(Machine(Machine_Reservation))==(carte_retiree,nombre_max,adresse_cl,nom_cl,num_carte,clients,num_k7,cassettes);
  Abstract_List_Variables(Machine(Machine_Reservation))==(?);
  Local_List_Variables(Machine(Machine_Reservation))==(date_res,reservation);
  List_Variables(Machine(Machine_Reservation))==(date_res,reservation);
  External_List_Variables(Machine(Machine_Reservation))==(date_res,reservation)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Machine_Reservation))==(?);
  Abstract_List_VisibleVariables(Machine(Machine_Reservation))==(?);
  External_List_VisibleVariables(Machine(Machine_Reservation))==(?);
  Expanded_List_VisibleVariables(Machine(Machine_Reservation))==(?);
  List_VisibleVariables(Machine(Machine_Reservation))==(?);
  Internal_List_VisibleVariables(Machine(Machine_Reservation))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Machine_Reservation))==(btrue);
  Gluing_List_Invariant(Machine(Machine_Reservation))==(reservation: cassettes --> clients & date_res: cassettes --> NAT);
  Expanded_List_Invariant(Machine(Machine_Reservation))==(btrue);
  Abstract_List_Invariant(Machine(Machine_Reservation))==(btrue);
  Context_List_Invariant(Machine(Machine_Reservation))==(clients <: CLIENTS & num_carte: clients >-> NAT & nom_cl: clients --> STRINGS & adresse_cl: clients --> STRINGS & nombre_max: clients --> NAT & carte_retiree: clients --> BOOL & cassettes <: CASSETTES & num_k7: cassettes --> NAT);
  List_Invariant(Machine(Machine_Reservation))==(btrue)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Machine_Reservation))==(btrue);
  Abstract_List_Assertions(Machine(Machine_Reservation))==(btrue);
  Context_List_Assertions(Machine(Machine_Reservation))==(btrue);
  List_Assertions(Machine(Machine_Reservation))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Machine_Reservation))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Machine_Reservation))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Machine_Reservation))==(reservation,date_res:={},{});
  Context_List_Initialisation(Machine(Machine_Reservation))==(clients,num_carte,nom_cl,adresse_cl,nombre_max,carte_retiree:={},{},{},{},{},{};cassettes,num_k7:={},{});
  List_Initialisation(Machine(Machine_Reservation))==(reservation,date_res:={},{})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Machine_Reservation))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Machine_Reservation),Machine(Machine_Client))==(?);
  List_Instanciated_Parameters(Machine(Machine_Reservation),Machine(Machine_Cassette))==(?);
  List_Instanciated_Parameters(Machine(Machine_Reservation),Machine(Context))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Machine_Reservation))==(btrue);
  List_Constraints(Machine(Machine_Reservation))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Machine_Reservation))==(ajout_reservation,suppression_reservation);
  List_Operations(Machine(Machine_Reservation))==(ajout_reservation,suppression_reservation)
END
&
THEORY ListInputX IS
  List_Input(Machine(Machine_Reservation),ajout_reservation)==(cl,k7,date);
  List_Input(Machine(Machine_Reservation),suppression_reservation)==(k7)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Machine_Reservation),ajout_reservation)==(?);
  List_Output(Machine(Machine_Reservation),suppression_reservation)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Machine_Reservation),ajout_reservation)==(ajout_reservation(cl,k7,date));
  List_Header(Machine(Machine_Reservation),suppression_reservation)==(suppression_reservation(k7))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Machine_Reservation),ajout_reservation)==(cl: clients & k7: cassettes-dom(reservation) & date: NAT & card(reservation~[{cl}])<nombre_max(cl));
  List_Precondition(Machine(Machine_Reservation),suppression_reservation)==(k7: dom(reservation))
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Machine_Reservation),suppression_reservation)==(k7: dom(reservation) | reservation,date_res:={k7}<<|reservation,{k7}<<|date_res);
  Expanded_List_Substitution(Machine(Machine_Reservation),ajout_reservation)==(cl: clients & k7: cassettes-dom(reservation) & date: NAT & card(reservation~[{cl}])<nombre_max(cl) | reservation,date_res:=reservation<+{k7|->cl},date_res<+{k7|->date});
  List_Substitution(Machine(Machine_Reservation),ajout_reservation)==(reservation(k7):=cl || date_res(k7):=date);
  List_Substitution(Machine(Machine_Reservation),suppression_reservation)==(reservation:={k7}<<|reservation || date_res:={k7}<<|date_res)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Machine_Reservation))==(?);
  Inherited_List_Constants(Machine(Machine_Reservation))==(?);
  List_Constants(Machine(Machine_Reservation))==(?)
END
&
THEORY ListSetsX IS
  Context_List_Enumerated(Machine(Machine_Reservation))==(?);
  Context_List_Defered(Machine(Machine_Reservation))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  Context_List_Sets(Machine(Machine_Reservation))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  List_Valuable_Sets(Machine(Machine_Reservation))==(?);
  Inherited_List_Enumerated(Machine(Machine_Reservation))==(?);
  Inherited_List_Defered(Machine(Machine_Reservation))==(?);
  Inherited_List_Sets(Machine(Machine_Reservation))==(?);
  List_Enumerated(Machine(Machine_Reservation))==(?);
  List_Defered(Machine(Machine_Reservation))==(?);
  List_Sets(Machine(Machine_Reservation))==(?)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Machine_Reservation))==(?);
  Expanded_List_HiddenConstants(Machine(Machine_Reservation))==(?);
  List_HiddenConstants(Machine(Machine_Reservation))==(?);
  External_List_HiddenConstants(Machine(Machine_Reservation))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Machine_Reservation))==(btrue);
  Context_List_Properties(Machine(Machine_Reservation))==(STRINGS: FIN(INTEGER) & not(STRINGS = {}) & CLIENTS: FIN(INTEGER) & not(CLIENTS = {}) & BOUTIQUES: FIN(INTEGER) & not(BOUTIQUES = {}) & FILMS: FIN(INTEGER) & not(FILMS = {}) & EPISODES: FIN(INTEGER) & not(EPISODES = {}) & CASSETTES: FIN(INTEGER) & not(CASSETTES = {}));
  Inherited_List_Properties(Machine(Machine_Reservation))==(btrue);
  List_Properties(Machine(Machine_Reservation))==(btrue)
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Machine_Reservation),Machine(Context))==(?);
  Seen_Context_List_Enumerated(Machine(Machine_Reservation))==(?);
  Seen_Context_List_Invariant(Machine(Machine_Reservation))==(btrue);
  Seen_Context_List_Assertions(Machine(Machine_Reservation))==(btrue);
  Seen_Context_List_Properties(Machine(Machine_Reservation))==(btrue);
  Seen_List_Constraints(Machine(Machine_Reservation))==(btrue);
  Seen_List_Operations(Machine(Machine_Reservation),Machine(Context))==(?);
  Seen_Expanded_List_Invariant(Machine(Machine_Reservation),Machine(Context))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Machine_Reservation),ajout_reservation)==(?);
  List_ANY_Var(Machine(Machine_Reservation),suppression_reservation)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Machine_Reservation)) == (? | ? | date_res,reservation | ? | ajout_reservation,suppression_reservation | ? | seen(Machine(Context)),used(Machine(Machine_Client)),used(Machine(Machine_Cassette)) | ? | Machine_Reservation);
  List_Of_HiddenCst_Ids(Machine(Machine_Reservation)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Reservation)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Reservation)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Reservation)) == (?: ?);
  List_Of_Ids(Machine(Machine_Cassette)) == (? | ? | num_k7,cassettes | ? | ajout_cassette,suppression_cassette | ? | seen(Machine(Context)) | ? | Machine_Cassette);
  List_Of_HiddenCst_Ids(Machine(Machine_Cassette)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Cassette)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Cassette)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Cassette)) == (?: ?);
  List_Of_Ids(Machine(Context)) == (STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES | ? | ? | ? | ? | ? | ? | ? | Context);
  List_Of_HiddenCst_Ids(Machine(Context)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Context)) == (?);
  List_Of_VisibleVar_Ids(Machine(Context)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Context)) == (?: ?);
  List_Of_Ids(Machine(Machine_Client)) == (? | ? | carte_retiree,nombre_max,adresse_cl,nom_cl,num_carte,clients | ? | ajout_client,suppression_client,retirer_carte | ? | seen(Machine(Context)) | ? | Machine_Client);
  List_Of_HiddenCst_Ids(Machine(Machine_Client)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Client)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Client)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Client)) == (?: ?)
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Machine_Reservation)) == (Type(date_res) == Mvl(SetOf(atype(CASSETTES,?,?)*btype(INTEGER,0,MAXINT)));Type(reservation) == Mvl(SetOf(atype(CASSETTES,?,?)*atype(CLIENTS,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Machine_Reservation)) == (Type(suppression_reservation) == Cst(No_type,atype(CASSETTES,?,?));Type(ajout_reservation) == Cst(No_type,atype(CLIENTS,?,?)*atype(CASSETTES,?,?)*btype(INTEGER,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
