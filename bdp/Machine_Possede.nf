﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Machine_Possede))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Machine_Possede))==(Machine(Machine_Possede));
  Level(Machine(Machine_Possede))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Machine_Possede)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Machine_Possede))==(Context)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Machine_Possede))==(Machine_Episode,Machine_Film)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Machine_Possede))==(?);
  List_Includes(Machine(Machine_Possede))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Machine_Possede))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Machine_Possede))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Machine_Possede))==(titre_ep,num_ep,episodes,genre,titre_fl,num_fl,films);
  Context_List_Variables(Machine(Machine_Possede))==(titre_ep,num_ep,episodes,genre,titre_fl,num_fl,films);
  Abstract_List_Variables(Machine(Machine_Possede))==(?);
  Local_List_Variables(Machine(Machine_Possede))==(possede);
  List_Variables(Machine(Machine_Possede))==(possede);
  External_List_Variables(Machine(Machine_Possede))==(possede)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Machine_Possede))==(?);
  Abstract_List_VisibleVariables(Machine(Machine_Possede))==(?);
  External_List_VisibleVariables(Machine(Machine_Possede))==(?);
  Expanded_List_VisibleVariables(Machine(Machine_Possede))==(?);
  List_VisibleVariables(Machine(Machine_Possede))==(?);
  Internal_List_VisibleVariables(Machine(Machine_Possede))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Machine_Possede))==(btrue);
  Gluing_List_Invariant(Machine(Machine_Possede))==(possede: episodes --> films);
  Expanded_List_Invariant(Machine(Machine_Possede))==(btrue);
  Abstract_List_Invariant(Machine(Machine_Possede))==(btrue);
  Context_List_Invariant(Machine(Machine_Possede))==(episodes <: EPISODES & num_ep: episodes --> NAT & titre_ep: episodes --> STRINGS & films <: FILMS & num_fl: films --> NAT & titre_fl: films --> STRINGS & genre: films --> STRINGS);
  List_Invariant(Machine(Machine_Possede))==(btrue)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Machine_Possede))==(btrue);
  Abstract_List_Assertions(Machine(Machine_Possede))==(btrue);
  Context_List_Assertions(Machine(Machine_Possede))==(btrue);
  List_Assertions(Machine(Machine_Possede))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Machine_Possede))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Machine_Possede))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Machine_Possede))==(possede:={});
  Context_List_Initialisation(Machine(Machine_Possede))==(episodes,num_ep,titre_ep:={},{},{};films,num_fl,titre_fl,genre:={},{},{},{});
  List_Initialisation(Machine(Machine_Possede))==(possede:={})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Machine_Possede))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Machine_Possede),Machine(Machine_Episode))==(?);
  List_Instanciated_Parameters(Machine(Machine_Possede),Machine(Machine_Film))==(?);
  List_Instanciated_Parameters(Machine(Machine_Possede),Machine(Context))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Machine_Possede))==(btrue);
  List_Constraints(Machine(Machine_Possede))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Machine_Possede))==(ajout_possede,suppression_possede);
  List_Operations(Machine(Machine_Possede))==(ajout_possede,suppression_possede)
END
&
THEORY ListInputX IS
  List_Input(Machine(Machine_Possede),ajout_possede)==(ep,fl);
  List_Input(Machine(Machine_Possede),suppression_possede)==(ep)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Machine_Possede),ajout_possede)==(?);
  List_Output(Machine(Machine_Possede),suppression_possede)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Machine_Possede),ajout_possede)==(ajout_possede(ep,fl));
  List_Header(Machine(Machine_Possede),suppression_possede)==(suppression_possede(ep))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Machine_Possede),ajout_possede)==(ep: episodes & fl: films);
  List_Precondition(Machine(Machine_Possede),suppression_possede)==(ep: dom(possede))
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Machine_Possede),suppression_possede)==(ep: dom(possede) | possede:={ep}<<|possede);
  Expanded_List_Substitution(Machine(Machine_Possede),ajout_possede)==(ep: episodes & fl: films | possede:=possede<+{ep|->fl});
  List_Substitution(Machine(Machine_Possede),ajout_possede)==(possede(ep):=fl);
  List_Substitution(Machine(Machine_Possede),suppression_possede)==(possede:={ep}<<|possede)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Machine_Possede))==(?);
  Inherited_List_Constants(Machine(Machine_Possede))==(?);
  List_Constants(Machine(Machine_Possede))==(?)
END
&
THEORY ListSetsX IS
  Context_List_Enumerated(Machine(Machine_Possede))==(?);
  Context_List_Defered(Machine(Machine_Possede))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  Context_List_Sets(Machine(Machine_Possede))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  List_Valuable_Sets(Machine(Machine_Possede))==(?);
  Inherited_List_Enumerated(Machine(Machine_Possede))==(?);
  Inherited_List_Defered(Machine(Machine_Possede))==(?);
  Inherited_List_Sets(Machine(Machine_Possede))==(?);
  List_Enumerated(Machine(Machine_Possede))==(?);
  List_Defered(Machine(Machine_Possede))==(?);
  List_Sets(Machine(Machine_Possede))==(?)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Machine_Possede))==(?);
  Expanded_List_HiddenConstants(Machine(Machine_Possede))==(?);
  List_HiddenConstants(Machine(Machine_Possede))==(?);
  External_List_HiddenConstants(Machine(Machine_Possede))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Machine_Possede))==(btrue);
  Context_List_Properties(Machine(Machine_Possede))==(STRINGS: FIN(INTEGER) & not(STRINGS = {}) & CLIENTS: FIN(INTEGER) & not(CLIENTS = {}) & BOUTIQUES: FIN(INTEGER) & not(BOUTIQUES = {}) & FILMS: FIN(INTEGER) & not(FILMS = {}) & EPISODES: FIN(INTEGER) & not(EPISODES = {}) & CASSETTES: FIN(INTEGER) & not(CASSETTES = {}));
  Inherited_List_Properties(Machine(Machine_Possede))==(btrue);
  List_Properties(Machine(Machine_Possede))==(btrue)
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Machine_Possede),Machine(Context))==(?);
  Seen_Context_List_Enumerated(Machine(Machine_Possede))==(?);
  Seen_Context_List_Invariant(Machine(Machine_Possede))==(btrue);
  Seen_Context_List_Assertions(Machine(Machine_Possede))==(btrue);
  Seen_Context_List_Properties(Machine(Machine_Possede))==(btrue);
  Seen_List_Constraints(Machine(Machine_Possede))==(btrue);
  Seen_List_Operations(Machine(Machine_Possede),Machine(Context))==(?);
  Seen_Expanded_List_Invariant(Machine(Machine_Possede),Machine(Context))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Machine_Possede),ajout_possede)==(?);
  List_ANY_Var(Machine(Machine_Possede),suppression_possede)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Machine_Possede)) == (? | ? | possede | ? | ajout_possede,suppression_possede | ? | seen(Machine(Context)),used(Machine(Machine_Episode)),used(Machine(Machine_Film)) | ? | Machine_Possede);
  List_Of_HiddenCst_Ids(Machine(Machine_Possede)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Possede)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Possede)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Possede)) == (?: ?);
  List_Of_Ids(Machine(Machine_Film)) == (? | ? | genre,titre_fl,num_fl,films | ? | ajout_film,suppression_film | ? | seen(Machine(Context)) | ? | Machine_Film);
  List_Of_HiddenCst_Ids(Machine(Machine_Film)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Film)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Film)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Film)) == (?: ?);
  List_Of_Ids(Machine(Context)) == (STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES | ? | ? | ? | ? | ? | ? | ? | Context);
  List_Of_HiddenCst_Ids(Machine(Context)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Context)) == (?);
  List_Of_VisibleVar_Ids(Machine(Context)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Context)) == (?: ?);
  List_Of_Ids(Machine(Machine_Episode)) == (? | ? | titre_ep,num_ep,episodes | ? | ajout_episode,suppression_episode | ? | seen(Machine(Context)) | ? | Machine_Episode);
  List_Of_HiddenCst_Ids(Machine(Machine_Episode)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Episode)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Episode)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Episode)) == (?: ?)
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Machine_Possede)) == (Type(possede) == Mvl(SetOf(atype(EPISODES,?,?)*atype(FILMS,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Machine_Possede)) == (Type(suppression_possede) == Cst(No_type,atype(EPISODES,?,?));Type(ajout_possede) == Cst(No_type,atype(EPISODES,?,?)*atype(FILMS,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
