﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Machine_Episode))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Machine_Episode))==(Machine(Machine_Episode));
  Level(Machine(Machine_Episode))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Machine_Episode)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Machine_Episode))==(Context)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Machine_Episode))==(?)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Machine_Episode))==(?);
  List_Includes(Machine(Machine_Episode))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Machine_Episode))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Machine_Episode))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Machine_Episode))==(?);
  Context_List_Variables(Machine(Machine_Episode))==(?);
  Abstract_List_Variables(Machine(Machine_Episode))==(?);
  Local_List_Variables(Machine(Machine_Episode))==(titre_ep,num_ep,episodes);
  List_Variables(Machine(Machine_Episode))==(titre_ep,num_ep,episodes);
  External_List_Variables(Machine(Machine_Episode))==(titre_ep,num_ep,episodes)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Machine_Episode))==(?);
  Abstract_List_VisibleVariables(Machine(Machine_Episode))==(?);
  External_List_VisibleVariables(Machine(Machine_Episode))==(?);
  Expanded_List_VisibleVariables(Machine(Machine_Episode))==(?);
  List_VisibleVariables(Machine(Machine_Episode))==(?);
  Internal_List_VisibleVariables(Machine(Machine_Episode))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Machine_Episode))==(btrue);
  Gluing_List_Invariant(Machine(Machine_Episode))==(btrue);
  Expanded_List_Invariant(Machine(Machine_Episode))==(btrue);
  Abstract_List_Invariant(Machine(Machine_Episode))==(btrue);
  Context_List_Invariant(Machine(Machine_Episode))==(btrue);
  List_Invariant(Machine(Machine_Episode))==(episodes <: EPISODES & num_ep: episodes --> NAT & titre_ep: episodes --> STRINGS)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Machine_Episode))==(btrue);
  Abstract_List_Assertions(Machine(Machine_Episode))==(btrue);
  Context_List_Assertions(Machine(Machine_Episode))==(btrue);
  List_Assertions(Machine(Machine_Episode))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Machine_Episode))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Machine_Episode))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Machine_Episode))==(episodes,num_ep,titre_ep:={},{},{});
  Context_List_Initialisation(Machine(Machine_Episode))==(skip);
  List_Initialisation(Machine(Machine_Episode))==(episodes,num_ep,titre_ep:={},{},{})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Machine_Episode))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Machine_Episode),Machine(Context))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Machine_Episode))==(btrue);
  List_Constraints(Machine(Machine_Episode))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Machine_Episode))==(ajout_episode,suppression_episode);
  List_Operations(Machine(Machine_Episode))==(ajout_episode,suppression_episode)
END
&
THEORY ListInputX IS
  List_Input(Machine(Machine_Episode),ajout_episode)==(ee,num,titre);
  List_Input(Machine(Machine_Episode),suppression_episode)==(ee)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Machine_Episode),ajout_episode)==(?);
  List_Output(Machine(Machine_Episode),suppression_episode)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Machine_Episode),ajout_episode)==(ajout_episode(ee,num,titre));
  List_Header(Machine(Machine_Episode),suppression_episode)==(suppression_episode(ee))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Machine_Episode),ajout_episode)==(ee: EPISODES-episodes & num: NAT & titre: STRINGS);
  List_Precondition(Machine(Machine_Episode),suppression_episode)==(ee: episodes)
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Machine_Episode),suppression_episode)==(ee: episodes | episodes,num_ep,titre_ep:=episodes-{ee},{ee}<<|num_ep,{ee}<<|titre_ep);
  Expanded_List_Substitution(Machine(Machine_Episode),ajout_episode)==(ee: EPISODES-episodes & num: NAT & titre: STRINGS | episodes,num_ep,titre_ep:=episodes\/{ee},num_ep<+{ee|->num},titre_ep<+{ee|->titre});
  List_Substitution(Machine(Machine_Episode),ajout_episode)==(episodes:=episodes\/{ee} || num_ep(ee):=num || titre_ep(ee):=titre);
  List_Substitution(Machine(Machine_Episode),suppression_episode)==(episodes:=episodes-{ee} || num_ep:={ee}<<|num_ep || titre_ep:={ee}<<|titre_ep)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Machine_Episode))==(?);
  Inherited_List_Constants(Machine(Machine_Episode))==(?);
  List_Constants(Machine(Machine_Episode))==(?)
END
&
THEORY ListSetsX IS
  Context_List_Enumerated(Machine(Machine_Episode))==(?);
  Context_List_Defered(Machine(Machine_Episode))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  Context_List_Sets(Machine(Machine_Episode))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  List_Valuable_Sets(Machine(Machine_Episode))==(?);
  Inherited_List_Enumerated(Machine(Machine_Episode))==(?);
  Inherited_List_Defered(Machine(Machine_Episode))==(?);
  Inherited_List_Sets(Machine(Machine_Episode))==(?);
  List_Enumerated(Machine(Machine_Episode))==(?);
  List_Defered(Machine(Machine_Episode))==(?);
  List_Sets(Machine(Machine_Episode))==(?)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Machine_Episode))==(?);
  Expanded_List_HiddenConstants(Machine(Machine_Episode))==(?);
  List_HiddenConstants(Machine(Machine_Episode))==(?);
  External_List_HiddenConstants(Machine(Machine_Episode))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Machine_Episode))==(btrue);
  Context_List_Properties(Machine(Machine_Episode))==(STRINGS: FIN(INTEGER) & not(STRINGS = {}) & CLIENTS: FIN(INTEGER) & not(CLIENTS = {}) & BOUTIQUES: FIN(INTEGER) & not(BOUTIQUES = {}) & FILMS: FIN(INTEGER) & not(FILMS = {}) & EPISODES: FIN(INTEGER) & not(EPISODES = {}) & CASSETTES: FIN(INTEGER) & not(CASSETTES = {}));
  Inherited_List_Properties(Machine(Machine_Episode))==(btrue);
  List_Properties(Machine(Machine_Episode))==(btrue)
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Machine_Episode),Machine(Context))==(?);
  Seen_Context_List_Enumerated(Machine(Machine_Episode))==(?);
  Seen_Context_List_Invariant(Machine(Machine_Episode))==(btrue);
  Seen_Context_List_Assertions(Machine(Machine_Episode))==(btrue);
  Seen_Context_List_Properties(Machine(Machine_Episode))==(btrue);
  Seen_List_Constraints(Machine(Machine_Episode))==(btrue);
  Seen_List_Operations(Machine(Machine_Episode),Machine(Context))==(?);
  Seen_Expanded_List_Invariant(Machine(Machine_Episode),Machine(Context))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Machine_Episode),ajout_episode)==(?);
  List_ANY_Var(Machine(Machine_Episode),suppression_episode)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Machine_Episode)) == (? | ? | titre_ep,num_ep,episodes | ? | ajout_episode,suppression_episode | ? | seen(Machine(Context)) | ? | Machine_Episode);
  List_Of_HiddenCst_Ids(Machine(Machine_Episode)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Episode)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Episode)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Episode)) == (?: ?);
  List_Of_Ids(Machine(Context)) == (STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES | ? | ? | ? | ? | ? | ? | ? | Context);
  List_Of_HiddenCst_Ids(Machine(Context)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Context)) == (?);
  List_Of_VisibleVar_Ids(Machine(Context)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Context)) == (?: ?)
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Machine_Episode)) == (Type(titre_ep) == Mvl(SetOf(atype(EPISODES,?,?)*atype(STRINGS,"[STRINGS","]STRINGS")));Type(num_ep) == Mvl(SetOf(atype(EPISODES,?,?)*btype(INTEGER,0,MAXINT)));Type(episodes) == Mvl(SetOf(atype(EPISODES,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Machine_Episode)) == (Type(suppression_episode) == Cst(No_type,atype(EPISODES,?,?));Type(ajout_episode) == Cst(No_type,atype(EPISODES,?,?)*btype(INTEGER,?,?)*atype(STRINGS,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
