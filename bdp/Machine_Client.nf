﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Machine_Client))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Machine_Client))==(Machine(Machine_Client));
  Level(Machine(Machine_Client))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Machine_Client)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Machine_Client))==(Context)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Machine_Client))==(?)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Machine_Client))==(?);
  List_Includes(Machine(Machine_Client))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Machine_Client))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Machine_Client))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Machine_Client))==(?);
  Context_List_Variables(Machine(Machine_Client))==(?);
  Abstract_List_Variables(Machine(Machine_Client))==(?);
  Local_List_Variables(Machine(Machine_Client))==(carte_retiree,nombre_max,adresse_cl,nom_cl,num_carte,clients);
  List_Variables(Machine(Machine_Client))==(carte_retiree,nombre_max,adresse_cl,nom_cl,num_carte,clients);
  External_List_Variables(Machine(Machine_Client))==(carte_retiree,nombre_max,adresse_cl,nom_cl,num_carte,clients)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Machine_Client))==(?);
  Abstract_List_VisibleVariables(Machine(Machine_Client))==(?);
  External_List_VisibleVariables(Machine(Machine_Client))==(?);
  Expanded_List_VisibleVariables(Machine(Machine_Client))==(?);
  List_VisibleVariables(Machine(Machine_Client))==(?);
  Internal_List_VisibleVariables(Machine(Machine_Client))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Machine_Client))==(btrue);
  Gluing_List_Invariant(Machine(Machine_Client))==(btrue);
  Expanded_List_Invariant(Machine(Machine_Client))==(btrue);
  Abstract_List_Invariant(Machine(Machine_Client))==(btrue);
  Context_List_Invariant(Machine(Machine_Client))==(btrue);
  List_Invariant(Machine(Machine_Client))==(clients <: CLIENTS & num_carte: clients >-> NAT & nom_cl: clients --> STRINGS & adresse_cl: clients --> STRINGS & nombre_max: clients --> NAT & carte_retiree: clients --> BOOL)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Machine_Client))==(btrue);
  Abstract_List_Assertions(Machine(Machine_Client))==(btrue);
  Context_List_Assertions(Machine(Machine_Client))==(btrue);
  List_Assertions(Machine(Machine_Client))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Machine_Client))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Machine_Client))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Machine_Client))==(clients,num_carte,nom_cl,adresse_cl,nombre_max,carte_retiree:={},{},{},{},{},{});
  Context_List_Initialisation(Machine(Machine_Client))==(skip);
  List_Initialisation(Machine(Machine_Client))==(clients,num_carte,nom_cl,adresse_cl,nombre_max,carte_retiree:={},{},{},{},{},{})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Machine_Client))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Machine_Client),Machine(Context))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Machine_Client))==(btrue);
  List_Constraints(Machine(Machine_Client))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Machine_Client))==(ajout_client,suppression_client,retirer_carte);
  List_Operations(Machine(Machine_Client))==(ajout_client,suppression_client,retirer_carte)
END
&
THEORY ListInputX IS
  List_Input(Machine(Machine_Client),ajout_client)==(cc,num,nom,adresse,caution,nbmax);
  List_Input(Machine(Machine_Client),suppression_client)==(cc);
  List_Input(Machine(Machine_Client),retirer_carte)==(cc)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Machine_Client),ajout_client)==(?);
  List_Output(Machine(Machine_Client),suppression_client)==(?);
  List_Output(Machine(Machine_Client),retirer_carte)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Machine_Client),ajout_client)==(ajout_client(cc,num,nom,adresse,caution,nbmax));
  List_Header(Machine(Machine_Client),suppression_client)==(suppression_client(cc));
  List_Header(Machine(Machine_Client),retirer_carte)==(retirer_carte(cc))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Machine_Client),ajout_client)==(cc: CLIENTS-clients & num: NAT & nom: STRINGS & adresse: STRINGS & caution: NAT & nbmax: NAT);
  List_Precondition(Machine(Machine_Client),suppression_client)==(cc: clients);
  List_Precondition(Machine(Machine_Client),retirer_carte)==(cc: clients)
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Machine_Client),retirer_carte)==(cc: clients | carte_retiree:=carte_retiree<+{cc|->TRUE});
  Expanded_List_Substitution(Machine(Machine_Client),suppression_client)==(cc: clients | clients,num_carte,nom_cl,adresse_cl,nombre_max,carte_retiree:=clients-{cc},{cc}<<|num_carte,{cc}<<|nom_cl,{cc}<<|adresse_cl,{cc}<<|nombre_max,{cc}<<|carte_retiree);
  Expanded_List_Substitution(Machine(Machine_Client),ajout_client)==(cc: CLIENTS-clients & num: NAT & nom: STRINGS & adresse: STRINGS & caution: NAT & nbmax: NAT | clients,num_carte,nom_cl,adresse_cl,nombre_max,carte_retiree:=clients\/{cc},num_carte<+{cc|->num},nom_cl<+{cc|->nom},adresse_cl<+{cc|->adresse},nombre_max<+{cc|->nbmax},carte_retiree<+{cc|->FALSE});
  List_Substitution(Machine(Machine_Client),ajout_client)==(clients:=clients\/{cc} || num_carte(cc):=num || nom_cl(cc):=nom || adresse_cl(cc):=adresse || nombre_max(cc):=nbmax || carte_retiree(cc):=FALSE);
  List_Substitution(Machine(Machine_Client),suppression_client)==(clients:=clients-{cc} || num_carte:={cc}<<|num_carte || nom_cl:={cc}<<|nom_cl || adresse_cl:={cc}<<|adresse_cl || nombre_max:={cc}<<|nombre_max || carte_retiree:={cc}<<|carte_retiree);
  List_Substitution(Machine(Machine_Client),retirer_carte)==(carte_retiree(cc):=TRUE)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Machine_Client))==(?);
  Inherited_List_Constants(Machine(Machine_Client))==(?);
  List_Constants(Machine(Machine_Client))==(?)
END
&
THEORY ListSetsX IS
  Context_List_Enumerated(Machine(Machine_Client))==(?);
  Context_List_Defered(Machine(Machine_Client))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  Context_List_Sets(Machine(Machine_Client))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  List_Valuable_Sets(Machine(Machine_Client))==(?);
  Inherited_List_Enumerated(Machine(Machine_Client))==(?);
  Inherited_List_Defered(Machine(Machine_Client))==(?);
  Inherited_List_Sets(Machine(Machine_Client))==(?);
  List_Enumerated(Machine(Machine_Client))==(?);
  List_Defered(Machine(Machine_Client))==(?);
  List_Sets(Machine(Machine_Client))==(?)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Machine_Client))==(?);
  Expanded_List_HiddenConstants(Machine(Machine_Client))==(?);
  List_HiddenConstants(Machine(Machine_Client))==(?);
  External_List_HiddenConstants(Machine(Machine_Client))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Machine_Client))==(btrue);
  Context_List_Properties(Machine(Machine_Client))==(STRINGS: FIN(INTEGER) & not(STRINGS = {}) & CLIENTS: FIN(INTEGER) & not(CLIENTS = {}) & BOUTIQUES: FIN(INTEGER) & not(BOUTIQUES = {}) & FILMS: FIN(INTEGER) & not(FILMS = {}) & EPISODES: FIN(INTEGER) & not(EPISODES = {}) & CASSETTES: FIN(INTEGER) & not(CASSETTES = {}));
  Inherited_List_Properties(Machine(Machine_Client))==(btrue);
  List_Properties(Machine(Machine_Client))==(btrue)
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Machine_Client),Machine(Context))==(?);
  Seen_Context_List_Enumerated(Machine(Machine_Client))==(?);
  Seen_Context_List_Invariant(Machine(Machine_Client))==(btrue);
  Seen_Context_List_Assertions(Machine(Machine_Client))==(btrue);
  Seen_Context_List_Properties(Machine(Machine_Client))==(btrue);
  Seen_List_Constraints(Machine(Machine_Client))==(btrue);
  Seen_List_Operations(Machine(Machine_Client),Machine(Context))==(?);
  Seen_Expanded_List_Invariant(Machine(Machine_Client),Machine(Context))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Machine_Client),ajout_client)==(?);
  List_ANY_Var(Machine(Machine_Client),suppression_client)==(?);
  List_ANY_Var(Machine(Machine_Client),retirer_carte)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Machine_Client)) == (? | ? | carte_retiree,nombre_max,adresse_cl,nom_cl,num_carte,clients | ? | ajout_client,suppression_client,retirer_carte | ? | seen(Machine(Context)) | ? | Machine_Client);
  List_Of_HiddenCst_Ids(Machine(Machine_Client)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Client)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Client)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Client)) == (?: ?);
  List_Of_Ids(Machine(Context)) == (STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES | ? | ? | ? | ? | ? | ? | ? | Context);
  List_Of_HiddenCst_Ids(Machine(Context)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Context)) == (?);
  List_Of_VisibleVar_Ids(Machine(Context)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Context)) == (?: ?)
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Machine_Client)) == (Type(carte_retiree) == Mvl(SetOf(atype(CLIENTS,?,?)*btype(BOOL,0,1)));Type(nombre_max) == Mvl(SetOf(atype(CLIENTS,?,?)*btype(INTEGER,0,MAXINT)));Type(adresse_cl) == Mvl(SetOf(atype(CLIENTS,?,?)*atype(STRINGS,"[STRINGS","]STRINGS")));Type(nom_cl) == Mvl(SetOf(atype(CLIENTS,?,?)*atype(STRINGS,"[STRINGS","]STRINGS")));Type(num_carte) == Mvl(SetOf(atype(CLIENTS,?,?)*btype(INTEGER,0,MAXINT)));Type(clients) == Mvl(SetOf(atype(CLIENTS,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Machine_Client)) == (Type(retirer_carte) == Cst(No_type,atype(CLIENTS,?,?));Type(suppression_client) == Cst(No_type,atype(CLIENTS,?,?));Type(ajout_client) == Cst(No_type,atype(CLIENTS,?,?)*btype(INTEGER,?,?)*atype(STRINGS,?,?)*atype(STRINGS,?,?)*btype(INTEGER,?,?)*btype(INTEGER,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
