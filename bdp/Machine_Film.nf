﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Machine_Film))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Machine_Film))==(Machine(Machine_Film));
  Level(Machine(Machine_Film))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Machine_Film)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Machine_Film))==(Context)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Machine_Film))==(?)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Machine_Film))==(?);
  List_Includes(Machine(Machine_Film))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Machine_Film))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Machine_Film))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Machine_Film))==(?);
  Context_List_Variables(Machine(Machine_Film))==(?);
  Abstract_List_Variables(Machine(Machine_Film))==(?);
  Local_List_Variables(Machine(Machine_Film))==(genre,titre_fl,num_fl,films);
  List_Variables(Machine(Machine_Film))==(genre,titre_fl,num_fl,films);
  External_List_Variables(Machine(Machine_Film))==(genre,titre_fl,num_fl,films)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Machine_Film))==(?);
  Abstract_List_VisibleVariables(Machine(Machine_Film))==(?);
  External_List_VisibleVariables(Machine(Machine_Film))==(?);
  Expanded_List_VisibleVariables(Machine(Machine_Film))==(?);
  List_VisibleVariables(Machine(Machine_Film))==(?);
  Internal_List_VisibleVariables(Machine(Machine_Film))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Machine_Film))==(btrue);
  Gluing_List_Invariant(Machine(Machine_Film))==(btrue);
  Expanded_List_Invariant(Machine(Machine_Film))==(btrue);
  Abstract_List_Invariant(Machine(Machine_Film))==(btrue);
  Context_List_Invariant(Machine(Machine_Film))==(btrue);
  List_Invariant(Machine(Machine_Film))==(films <: FILMS & num_fl: films --> NAT & titre_fl: films --> STRINGS & genre: films --> STRINGS)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Machine_Film))==(btrue);
  Abstract_List_Assertions(Machine(Machine_Film))==(btrue);
  Context_List_Assertions(Machine(Machine_Film))==(btrue);
  List_Assertions(Machine(Machine_Film))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Machine_Film))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Machine_Film))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Machine_Film))==(films,num_fl,titre_fl,genre:={},{},{},{});
  Context_List_Initialisation(Machine(Machine_Film))==(skip);
  List_Initialisation(Machine(Machine_Film))==(films,num_fl,titre_fl,genre:={},{},{},{})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Machine_Film))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Machine_Film),Machine(Context))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Machine_Film))==(btrue);
  List_Constraints(Machine(Machine_Film))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Machine_Film))==(ajout_film,suppression_film);
  List_Operations(Machine(Machine_Film))==(ajout_film,suppression_film)
END
&
THEORY ListInputX IS
  List_Input(Machine(Machine_Film),ajout_film)==(ff,num,titre,gen);
  List_Input(Machine(Machine_Film),suppression_film)==(ff)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Machine_Film),ajout_film)==(?);
  List_Output(Machine(Machine_Film),suppression_film)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Machine_Film),ajout_film)==(ajout_film(ff,num,titre,gen));
  List_Header(Machine(Machine_Film),suppression_film)==(suppression_film(ff))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Machine_Film),ajout_film)==(ff: FILMS-films & num: NAT & titre: STRINGS & gen: STRINGS);
  List_Precondition(Machine(Machine_Film),suppression_film)==(ff: films)
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Machine_Film),suppression_film)==(ff: films | films,num_fl,titre_fl,genre:=films-{ff},{ff}<<|num_fl,{ff}<<|titre_fl,{ff}<<|genre);
  Expanded_List_Substitution(Machine(Machine_Film),ajout_film)==(ff: FILMS-films & num: NAT & titre: STRINGS & gen: STRINGS | films,num_fl,titre_fl,genre:=films\/{ff},num_fl<+{ff|->num},titre_fl<+{ff|->titre},genre<+{ff|->gen});
  List_Substitution(Machine(Machine_Film),ajout_film)==(films:=films\/{ff} || num_fl(ff):=num || titre_fl(ff):=titre || genre(ff):=gen);
  List_Substitution(Machine(Machine_Film),suppression_film)==(films:=films-{ff} || num_fl:={ff}<<|num_fl || titre_fl:={ff}<<|titre_fl || genre:={ff}<<|genre)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Machine_Film))==(?);
  Inherited_List_Constants(Machine(Machine_Film))==(?);
  List_Constants(Machine(Machine_Film))==(?)
END
&
THEORY ListSetsX IS
  Context_List_Enumerated(Machine(Machine_Film))==(?);
  Context_List_Defered(Machine(Machine_Film))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  Context_List_Sets(Machine(Machine_Film))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  List_Valuable_Sets(Machine(Machine_Film))==(?);
  Inherited_List_Enumerated(Machine(Machine_Film))==(?);
  Inherited_List_Defered(Machine(Machine_Film))==(?);
  Inherited_List_Sets(Machine(Machine_Film))==(?);
  List_Enumerated(Machine(Machine_Film))==(?);
  List_Defered(Machine(Machine_Film))==(?);
  List_Sets(Machine(Machine_Film))==(?)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Machine_Film))==(?);
  Expanded_List_HiddenConstants(Machine(Machine_Film))==(?);
  List_HiddenConstants(Machine(Machine_Film))==(?);
  External_List_HiddenConstants(Machine(Machine_Film))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Machine_Film))==(btrue);
  Context_List_Properties(Machine(Machine_Film))==(STRINGS: FIN(INTEGER) & not(STRINGS = {}) & CLIENTS: FIN(INTEGER) & not(CLIENTS = {}) & BOUTIQUES: FIN(INTEGER) & not(BOUTIQUES = {}) & FILMS: FIN(INTEGER) & not(FILMS = {}) & EPISODES: FIN(INTEGER) & not(EPISODES = {}) & CASSETTES: FIN(INTEGER) & not(CASSETTES = {}));
  Inherited_List_Properties(Machine(Machine_Film))==(btrue);
  List_Properties(Machine(Machine_Film))==(btrue)
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Machine_Film),Machine(Context))==(?);
  Seen_Context_List_Enumerated(Machine(Machine_Film))==(?);
  Seen_Context_List_Invariant(Machine(Machine_Film))==(btrue);
  Seen_Context_List_Assertions(Machine(Machine_Film))==(btrue);
  Seen_Context_List_Properties(Machine(Machine_Film))==(btrue);
  Seen_List_Constraints(Machine(Machine_Film))==(btrue);
  Seen_List_Operations(Machine(Machine_Film),Machine(Context))==(?);
  Seen_Expanded_List_Invariant(Machine(Machine_Film),Machine(Context))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Machine_Film),ajout_film)==(?);
  List_ANY_Var(Machine(Machine_Film),suppression_film)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Machine_Film)) == (? | ? | genre,titre_fl,num_fl,films | ? | ajout_film,suppression_film | ? | seen(Machine(Context)) | ? | Machine_Film);
  List_Of_HiddenCst_Ids(Machine(Machine_Film)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Film)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Film)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Film)) == (?: ?);
  List_Of_Ids(Machine(Context)) == (STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES | ? | ? | ? | ? | ? | ? | ? | Context);
  List_Of_HiddenCst_Ids(Machine(Context)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Context)) == (?);
  List_Of_VisibleVar_Ids(Machine(Context)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Context)) == (?: ?)
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Machine_Film)) == (Type(genre) == Mvl(SetOf(atype(FILMS,?,?)*atype(STRINGS,"[STRINGS","]STRINGS")));Type(titre_fl) == Mvl(SetOf(atype(FILMS,?,?)*atype(STRINGS,"[STRINGS","]STRINGS")));Type(num_fl) == Mvl(SetOf(atype(FILMS,?,?)*btype(INTEGER,0,MAXINT)));Type(films) == Mvl(SetOf(atype(FILMS,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Machine_Film)) == (Type(suppression_film) == Cst(No_type,atype(FILMS,?,?));Type(ajout_film) == Cst(No_type,atype(FILMS,?,?)*btype(INTEGER,?,?)*atype(STRINGS,?,?)*atype(STRINGS,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
