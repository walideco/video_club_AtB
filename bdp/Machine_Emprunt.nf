﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(Machine_Emprunt))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(Machine_Emprunt))==(Machine(Machine_Emprunt));
  Level(Machine(Machine_Emprunt))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(Machine_Emprunt)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(Machine_Emprunt))==(Context)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(Machine_Emprunt))==(Machine_Client,Machine_Cassette)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(Machine_Emprunt))==(?);
  List_Includes(Machine(Machine_Emprunt))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(Machine_Emprunt))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(Machine_Emprunt))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(Machine_Emprunt))==(carte_retiree,nombre_max,adresse_cl,nom_cl,num_carte,clients,num_k7,cassettes);
  Context_List_Variables(Machine(Machine_Emprunt))==(carte_retiree,nombre_max,adresse_cl,nom_cl,num_carte,clients,num_k7,cassettes);
  Abstract_List_Variables(Machine(Machine_Emprunt))==(?);
  Local_List_Variables(Machine(Machine_Emprunt))==(date_emp,emprunt);
  List_Variables(Machine(Machine_Emprunt))==(date_emp,emprunt);
  External_List_Variables(Machine(Machine_Emprunt))==(date_emp,emprunt)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(Machine_Emprunt))==(?);
  Abstract_List_VisibleVariables(Machine(Machine_Emprunt))==(?);
  External_List_VisibleVariables(Machine(Machine_Emprunt))==(?);
  Expanded_List_VisibleVariables(Machine(Machine_Emprunt))==(?);
  List_VisibleVariables(Machine(Machine_Emprunt))==(?);
  Internal_List_VisibleVariables(Machine(Machine_Emprunt))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(Machine_Emprunt))==(btrue);
  Gluing_List_Invariant(Machine(Machine_Emprunt))==(emprunt: cassettes --> clients & date_emp: cassettes --> NAT);
  Expanded_List_Invariant(Machine(Machine_Emprunt))==(btrue);
  Abstract_List_Invariant(Machine(Machine_Emprunt))==(btrue);
  Context_List_Invariant(Machine(Machine_Emprunt))==(clients <: CLIENTS & num_carte: clients >-> NAT & nom_cl: clients --> STRINGS & adresse_cl: clients --> STRINGS & nombre_max: clients --> NAT & carte_retiree: clients --> BOOL & cassettes <: CASSETTES & num_k7: cassettes --> NAT);
  List_Invariant(Machine(Machine_Emprunt))==(btrue)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(Machine_Emprunt))==(btrue);
  Abstract_List_Assertions(Machine(Machine_Emprunt))==(btrue);
  Context_List_Assertions(Machine(Machine_Emprunt))==(btrue);
  List_Assertions(Machine(Machine_Emprunt))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(Machine_Emprunt))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(Machine_Emprunt))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(Machine_Emprunt))==(emprunt,date_emp:={},{});
  Context_List_Initialisation(Machine(Machine_Emprunt))==(clients,num_carte,nom_cl,adresse_cl,nombre_max,carte_retiree:={},{},{},{},{},{};cassettes,num_k7:={},{});
  List_Initialisation(Machine(Machine_Emprunt))==(emprunt,date_emp:={},{})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(Machine_Emprunt))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(Machine_Emprunt),Machine(Machine_Client))==(?);
  List_Instanciated_Parameters(Machine(Machine_Emprunt),Machine(Machine_Cassette))==(?);
  List_Instanciated_Parameters(Machine(Machine_Emprunt),Machine(Context))==(?)
END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(Machine_Emprunt))==(btrue);
  List_Constraints(Machine(Machine_Emprunt))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(Machine_Emprunt))==(ajout_emprunt,suppression_emprunt);
  List_Operations(Machine(Machine_Emprunt))==(ajout_emprunt,suppression_emprunt)
END
&
THEORY ListInputX IS
  List_Input(Machine(Machine_Emprunt),ajout_emprunt)==(cl,k7,date);
  List_Input(Machine(Machine_Emprunt),suppression_emprunt)==(k7)
END
&
THEORY ListOutputX IS
  List_Output(Machine(Machine_Emprunt),ajout_emprunt)==(?);
  List_Output(Machine(Machine_Emprunt),suppression_emprunt)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(Machine_Emprunt),ajout_emprunt)==(ajout_emprunt(cl,k7,date));
  List_Header(Machine(Machine_Emprunt),suppression_emprunt)==(suppression_emprunt(k7))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(Machine_Emprunt),ajout_emprunt)==(cl: clients & k7: cassettes-dom(emprunt) & date: NAT & card(emprunt~[{cl}])<nombre_max(cl));
  List_Precondition(Machine(Machine_Emprunt),suppression_emprunt)==(k7: dom(emprunt))
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(Machine_Emprunt),suppression_emprunt)==(k7: dom(emprunt) | emprunt,date_emp:={k7}<<|emprunt,{k7}<<|date_emp);
  Expanded_List_Substitution(Machine(Machine_Emprunt),ajout_emprunt)==(cl: clients & k7: cassettes-dom(emprunt) & date: NAT & card(emprunt~[{cl}])<nombre_max(cl) | emprunt,date_emp:=emprunt<+{k7|->cl},date_emp<+{k7|->date});
  List_Substitution(Machine(Machine_Emprunt),ajout_emprunt)==(emprunt(k7):=cl || date_emp(k7):=date);
  List_Substitution(Machine(Machine_Emprunt),suppression_emprunt)==(emprunt:={k7}<<|emprunt || date_emp:={k7}<<|date_emp)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(Machine_Emprunt))==(?);
  Inherited_List_Constants(Machine(Machine_Emprunt))==(?);
  List_Constants(Machine(Machine_Emprunt))==(?)
END
&
THEORY ListSetsX IS
  Context_List_Enumerated(Machine(Machine_Emprunt))==(?);
  Context_List_Defered(Machine(Machine_Emprunt))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  Context_List_Sets(Machine(Machine_Emprunt))==(STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES);
  List_Valuable_Sets(Machine(Machine_Emprunt))==(?);
  Inherited_List_Enumerated(Machine(Machine_Emprunt))==(?);
  Inherited_List_Defered(Machine(Machine_Emprunt))==(?);
  Inherited_List_Sets(Machine(Machine_Emprunt))==(?);
  List_Enumerated(Machine(Machine_Emprunt))==(?);
  List_Defered(Machine(Machine_Emprunt))==(?);
  List_Sets(Machine(Machine_Emprunt))==(?)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(Machine_Emprunt))==(?);
  Expanded_List_HiddenConstants(Machine(Machine_Emprunt))==(?);
  List_HiddenConstants(Machine(Machine_Emprunt))==(?);
  External_List_HiddenConstants(Machine(Machine_Emprunt))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(Machine_Emprunt))==(btrue);
  Context_List_Properties(Machine(Machine_Emprunt))==(STRINGS: FIN(INTEGER) & not(STRINGS = {}) & CLIENTS: FIN(INTEGER) & not(CLIENTS = {}) & BOUTIQUES: FIN(INTEGER) & not(BOUTIQUES = {}) & FILMS: FIN(INTEGER) & not(FILMS = {}) & EPISODES: FIN(INTEGER) & not(EPISODES = {}) & CASSETTES: FIN(INTEGER) & not(CASSETTES = {}));
  Inherited_List_Properties(Machine(Machine_Emprunt))==(btrue);
  List_Properties(Machine(Machine_Emprunt))==(btrue)
END
&
THEORY ListSeenInfoX IS
  Seen_Internal_List_Operations(Machine(Machine_Emprunt),Machine(Context))==(?);
  Seen_Context_List_Enumerated(Machine(Machine_Emprunt))==(?);
  Seen_Context_List_Invariant(Machine(Machine_Emprunt))==(btrue);
  Seen_Context_List_Assertions(Machine(Machine_Emprunt))==(btrue);
  Seen_Context_List_Properties(Machine(Machine_Emprunt))==(btrue);
  Seen_List_Constraints(Machine(Machine_Emprunt))==(btrue);
  Seen_List_Operations(Machine(Machine_Emprunt),Machine(Context))==(?);
  Seen_Expanded_List_Invariant(Machine(Machine_Emprunt),Machine(Context))==(btrue)
END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(Machine_Emprunt),ajout_emprunt)==(?);
  List_ANY_Var(Machine(Machine_Emprunt),suppression_emprunt)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(Machine_Emprunt)) == (? | ? | date_emp,emprunt | ? | ajout_emprunt,suppression_emprunt | ? | seen(Machine(Context)),used(Machine(Machine_Client)),used(Machine(Machine_Cassette)) | ? | Machine_Emprunt);
  List_Of_HiddenCst_Ids(Machine(Machine_Emprunt)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Emprunt)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Emprunt)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Emprunt)) == (?: ?);
  List_Of_Ids(Machine(Machine_Cassette)) == (? | ? | num_k7,cassettes | ? | ajout_cassette,suppression_cassette | ? | seen(Machine(Context)) | ? | Machine_Cassette);
  List_Of_HiddenCst_Ids(Machine(Machine_Cassette)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Cassette)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Cassette)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Cassette)) == (?: ?);
  List_Of_Ids(Machine(Context)) == (STRINGS,CLIENTS,BOUTIQUES,FILMS,EPISODES,CASSETTES | ? | ? | ? | ? | ? | ? | ? | Context);
  List_Of_HiddenCst_Ids(Machine(Context)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Context)) == (?);
  List_Of_VisibleVar_Ids(Machine(Context)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Context)) == (?: ?);
  List_Of_Ids(Machine(Machine_Client)) == (? | ? | carte_retiree,nombre_max,adresse_cl,nom_cl,num_carte,clients | ? | ajout_client,suppression_client,retirer_carte | ? | seen(Machine(Context)) | ? | Machine_Client);
  List_Of_HiddenCst_Ids(Machine(Machine_Client)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(Machine_Client)) == (?);
  List_Of_VisibleVar_Ids(Machine(Machine_Client)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(Machine_Client)) == (?: ?)
END
&
THEORY VariablesEnvX IS
  Variables(Machine(Machine_Emprunt)) == (Type(date_emp) == Mvl(SetOf(atype(CASSETTES,?,?)*btype(INTEGER,0,MAXINT)));Type(emprunt) == Mvl(SetOf(atype(CASSETTES,?,?)*atype(CLIENTS,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(Machine_Emprunt)) == (Type(suppression_emprunt) == Cst(No_type,atype(CASSETTES,?,?));Type(ajout_emprunt) == Cst(No_type,atype(CLIENTS,?,?)*atype(CASSETTES,?,?)*btype(INTEGER,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
